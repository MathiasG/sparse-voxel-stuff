use crate::{
    hash_dag::{constants::SUPPORTED_LEVELS, utils::vptr_to_lvl},
    shared_xform::SharedTransform,
    utils::new_model_xform,
};
use ::{
    nalgebra::SimdRealField,
    std::ops::{Deref, DerefMut},
};

pub struct Model<N: SimdRealField = f32> {
    pub vptr: u32,
    pub xform: SharedTransform<N>,
    pub changed: bool,
}

impl<N: SimdRealField> Model<N> {
    #[inline]
    pub fn new<I: Into<SharedTransform<N>>>(vptr: u32, xform: I) -> Self {
        Self {
            vptr,
            xform: xform.into(),
            changed: true,
        }
    }
    #[inline]
    pub fn update(&mut self, vptr: u32) {
        self.changed = self.vptr != vptr;
        self.vptr = vptr;
    }
    #[inline]
    pub fn level(&self) -> u32 {
        vptr_to_lvl(self.vptr)
    }
    #[inline]
    pub fn levels(&self) -> u32 {
        SUPPORTED_LEVELS - self.level()
    }
}

impl Model {
    #[inline]
    pub fn from_vptr(vptr: u32) -> Self {
        Self::new(vptr, new_model_xform(SUPPORTED_LEVELS - vptr_to_lvl(vptr)))
    }
}

impl<N: SimdRealField> Deref for Model<N> {
    type Target = SharedTransform<N>;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.xform
    }
}

impl<N: SimdRealField> DerefMut for Model<N> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.xform
    }
}
