# Tools

## Analytics

Example (through action):
1. Run the default demo (which opens a HashDAG named "main").
2. As the HashDAG is initializing (or at any-time after) run something like: ```./tools/analyse.py -n main -t bob```

The idea is that tools can be written rather easily and be hooked up under a single interface. It's supposed to be very simple for 2 reasons:
1. I'd like it to be inviting for anyone, no matter the expertise.
2. I want quick iteration times.

**Note:** One can use multiple tools by running multiple instances at once. There is no lock on resources.

### Safety

At the moment there are a couple of things I'm aware of can be done better. For instance:
- Anything unrecoverable in the main application leads to a memory-leak of shared memory.
- Programs reading the data have no way to know if the main application shut down and therefore can read de-allocated memory.
- There is no thread-safety which may be fine if you don't expect 100% accuracy, but it may also cause trouble with heavy operations.
- There is nothing involving permissions, so anything can read, write and de-allocate memory.

It's nasty and somewhat shameful, but the kind of demographic that would see this (which is nobody) is the best kind to deal with it.

### Usage example:

1. You ran `./tools/find_hash_dag.py` and confirmed "main" is available.
2. You want to know how many pages are allocated per bucket for each level, so you figure that could be "bucket_occupancy_bar" or "bob" by running `./tools/query_toolkit.py`.
3. Finally you run: ```./tools/analyse.py -n main -t bob```.
4. You're absolutely blown away by its magnificence. A moment to never forget.

There is more. Most notably there is option "-a":

You want to change how buckets are binned. So you run: ```./tools/analyse.py -n main -t bob -a "binning=mean, bins=20"```.

### Extending analytics

This is just in case you're curious. Don't take this as a tutorial.

Say you want to extend `BasicHashDAG`'s tooling. For that, all you need to know is:
- That all its current tooling is located in the `BasicHashDAG` module.
- There are some assumptions about the function you'd write. You can deduce what they are from `function_arguments` in the `toolkit` module (it's a tiny function with 3 conditions).
- You'd need to add it to the `toolkit` module in the `toolkit` map.

### Limitations

Limitations lie in how simple things are. It's just a chunk of memory and a configuration file for mercy. There is no such thing as an event and all that is possible is polling for changes. That implicates that things like tracking changes (which could serve really well for analytics) or benchmarking is not possible as-is.