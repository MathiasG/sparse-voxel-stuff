use self::{
    buffer::Buffer,
    context::VkContext,
    debug::{
        check_validation_layer_support, get_layer_names_and_pointers, ENABLE_VALIDATION_LAYERS,
    },
    swapchain::{SwapchainProperties, SwapchainSupportDetails},
};
use super::{
    app_info::NAME,
    cpu_rt::RTCache,
    hash_dag::{
        constants::{LEAF_LEVEL, PAGE_LEN, SUPPORTED_LEVELS, TOTAL_PAGES},
        shared_hash_dag::SharedHashDAG,
        staging::Staging,
        tracking::{basic::*, Tracker},
        HashDAG,
    },
};
use ::{
    ash::{
        extensions::{
            ext::DebugReport,
            khr::{Surface, Swapchain},
        },
        util::{read_spv, Align},
        version::{DeviceV1_0, EntryV1_0, InstanceV1_0},
        vk, Device, Entry, Instance,
    },
    std::{
        ffi::{CStr, CString},
        fs::File,
        io::{Cursor, Read},
        mem::{align_of, size_of},
        ops::Range,
        os::raw::c_char,
        path::Path,
        slice::from_raw_parts_mut,
        time::SystemTime,
    },
    winit::window::Window,
};

pub mod buffer;
pub mod context;
pub mod debug;
pub mod swapchain;

macro_rules! data_to_map_entries {
    ($start_id: expr, $start_offset: expr, $data: expr) => {{
        let mut map_entries = Vec::with_capacity($data.len());
        let (mut constant_id, mut offset) = ($start_id, $start_offset);
        for bytes in $data.iter() {
            map_entries.push(vk::SpecializationMapEntry {
                constant_id,
                offset,
                size: bytes.len(),
            });
            constant_id += 1;
            offset += bytes.len() as u32;
        }
        map_entries
    }};
}

pub const LOCAL: [u32; 2] = [16, 8];

#[inline]
/// This method is unsafe for a number of reasons:
///
/// * There is no guarantee to the validity of `ptr`.
/// * The returned lifetime is not guaranteed to be the actual lifetime of
///   `ptr`.
/// * There is no guarantee that the memory pointed to by `ptr` contains a
///   valid nul terminator byte at the end of the string.
/// * It is not guaranteed that the memory pointed by `ptr` won't change
///   before the `CStr` has been destroyed.
pub unsafe fn name_to_c_str(name: [c_char; vk::MAX_EXTENSION_NAME_SIZE]) -> &'static CStr {
    CStr::from_ptr(name.as_ptr())
}

pub mod bindings {
    pub const RENDER: u32 = 0;
    pub const POOL: u32 = 1;
    pub const PAGE_LUT: u32 = 2;
    pub const STATIC: u32 = 3;
    pub const MODEL: u32 = 4;
}

pub struct Renderer {
    pub vk_context: VkContext,
    pub queue_families_indices: QueueFamiliesIndices,
    pub compute_queue: vk::Queue,
    pub present_queue: vk::Queue,
    pub swapchain: Swapchain,
    pub swapchain_khr: vk::SwapchainKHR,
    pub swapchain_properties: SwapchainProperties,
    pub image_idx: usize,
    pub swapchain_images: Vec<vk::Image>,
    pub swapchain_image_views: Vec<vk::ImageView>,
    pub descriptor_set_layout: vk::DescriptorSetLayout,
    pub pipeline_layout: vk::PipelineLayout,
    pub pipeline: vk::Pipeline,
    pub command_pool: vk::CommandPool,
    pub pool_buf: Buffer,
    pub page_table_buf: Buffer,
    pub static_buf: Buffer,
    pub model_buf: Buffer,
    pub descriptor_pool: vk::DescriptorPool,
    pub descriptor_sets: Vec<vk::DescriptorSet>,
    pub command_buffers: Vec<vk::CommandBuffer>,

    pub image_available: vk::Semaphore,
    pub render_finished: vk::Semaphore,
    pub fence: vk::Fence,

    pub rt_cache: RTCache,
    pub resize: Option<vk::Extent2D>,
    pub fov: f32,
    pub aperture: f32,
    pub window: Window,
}

impl Renderer {
    pub fn new_instance(entry: &Entry, window: &Window) -> Instance {
        let app_name = CString::new(NAME).unwrap();
        let engine_name = CString::new("No Engine").unwrap();
        let app_info = vk::ApplicationInfo::builder()
            .application_name(app_name.as_c_str())
            .application_version(vk::make_version(0, 1, 0))
            .engine_name(engine_name.as_c_str())
            .engine_version(vk::make_version(0, 1, 0))
            .api_version(vk::make_version(1, 0, 0))
            .build();

        let extension_names = ash_window::enumerate_required_extensions(window).unwrap();
        let mut extension_names = extension_names
            .iter()
            .map(|ext| ext.as_ptr())
            .collect::<Vec<_>>();
        if ENABLE_VALIDATION_LAYERS {
            extension_names.push(DebugReport::name().as_ptr());
        }

        let instance_create_info_builder = vk::InstanceCreateInfo::builder()
            .application_info(&app_info)
            .enabled_extension_names(&extension_names);
        let (_layer_names, layer_names_ptrs) = get_layer_names_and_pointers();
        let instance_create_info = if ENABLE_VALIDATION_LAYERS {
            check_validation_layer_support(&entry);
            instance_create_info_builder.enabled_layer_names(&layer_names_ptrs)
        } else {
            instance_create_info_builder
        }
        .build();

        unsafe { entry.create_instance(&instance_create_info, None).unwrap() }
    }

    fn find_queue_families(
        instance: &Instance,
        surface: &Surface,
        surface_khr: vk::SurfaceKHR,
        device: vk::PhysicalDevice,
    ) -> (Option<u32>, Option<u32>) {
        let (mut compute, mut present) = (None, None);
        let props = unsafe { instance.get_physical_device_queue_family_properties(device) };
        for (index, family) in props.iter().filter(|f| f.queue_count > 0).enumerate() {
            let index = index as u32;

            if family.queue_flags.contains(vk::QueueFlags::COMPUTE) && compute.is_none() {
                compute = Some(index);
            }
            let present_support =
                unsafe { surface.get_physical_device_surface_support(device, index, surface_khr) };
            if present_support == Ok(true) && present.is_none() {
                present = Some(index);
            }

            if compute.is_some() && present.is_some() {
                break;
            }
        }
        (compute, present)
    }

    #[inline]
    fn get_required_device_extensions() -> Vec<&'static CStr> {
        vec![Swapchain::name()]
    }

    fn check_device_extension_support(instance: &Instance, device: vk::PhysicalDevice) -> bool {
        let mut required_ext = Self::get_required_device_extensions();
        for ext in unsafe { instance.enumerate_device_extension_properties(device) }.unwrap() {
            let is_eq = |&x| unsafe { name_to_c_str(ext.extension_name) } == x;
            if let Some(index) = required_ext.iter().position(is_eq) {
                required_ext.remove(index);
            }
        }
        required_ext.is_empty()
    }

    fn is_device_suitable(
        instance: &Instance,
        surface: &Surface,
        surface_khr: vk::SurfaceKHR,
        device: vk::PhysicalDevice,
    ) -> bool {
        let (compute, present) = Self::find_queue_families(instance, surface, surface_khr, device);
        let extension_support = Self::check_device_extension_support(instance, device);
        let is_swapchain_adequate = {
            let details = unsafe { SwapchainSupportDetails::new(device, surface, surface_khr) };
            !details.formats.is_empty() && !details.present_modes.is_empty()
        };
        compute.is_some() && present.is_some() && extension_support && is_swapchain_adequate
    }

    pub fn lets_get_physical(
        instance: &Instance,
        surface: &Surface,
        surface_khr: vk::SurfaceKHR,
    ) -> (vk::PhysicalDevice, QueueFamiliesIndices) {
        let devices = unsafe { instance.enumerate_physical_devices() }.unwrap();
        let device = devices
            .into_iter()
            .find(|device| Self::is_device_suitable(instance, surface, surface_khr, *device))
            .expect("No suitable physical device.");
        let props = unsafe { instance.get_physical_device_properties(device) };
        log::debug!("Selected physical device: {:?}", unsafe {
            CStr::from_ptr(props.device_name.as_ptr())
        });
        let (compute, present) = Self::find_queue_families(instance, surface, surface_khr, device);
        let queue_families_indices = QueueFamiliesIndices {
            compute_index: compute.expect("Could not find a queue family for `compute`"),
            present_index: present.expect("Could not find a queue family for `present`"),
        };
        log::debug!("Selected queue indices.");
        (device, queue_families_indices)
    }

    pub fn create_logical_device(
        instance: &Instance,
        device: vk::PhysicalDevice,
        QueueFamiliesIndices {
            compute_index,
            present_index,
        }: QueueFamiliesIndices,
    ) -> (Device, vk::Queue, vk::Queue) {
        let queue_priorities = [1.0f32];
        let queue_create_infos: Vec<_> = {
            let mut indices = vec![compute_index, present_index];
            // Vulkan specs does not allow passing an array containing duplicated family indices.
            // And the future or current implementation might have indices point to a shared family.
            indices.dedup();
            let to_queue_info = |index: &u32| {
                vk::DeviceQueueCreateInfo::builder()
                    .queue_family_index(*index)
                    .queue_priorities(&queue_priorities)
                    .build()
            };
            indices.iter().map(to_queue_info).collect()
        };
        let device_extensions = Self::get_required_device_extensions();
        let device_extensions_ptrs: Vec<_> =
            device_extensions.iter().map(|ext| ext.as_ptr()).collect();
        let device_features = vk::PhysicalDeviceFeatures::builder().build();
        let (_layer_names, layer_names_ptrs) = get_layer_names_and_pointers();
        let mut device_create_info_builder = vk::DeviceCreateInfo::builder()
            .queue_create_infos(&queue_create_infos)
            .enabled_extension_names(&device_extensions_ptrs)
            .enabled_features(&device_features);
        if ENABLE_VALIDATION_LAYERS {
            device_create_info_builder =
                device_create_info_builder.enabled_layer_names(&layer_names_ptrs)
        }
        let device_create_info = device_create_info_builder.build();
        let device = unsafe { instance.create_device(device, &device_create_info, None) }
            .expect("Failed to create logical device.");
        let compute_queue = unsafe { device.get_device_queue(compute_index, 0) };
        let present_queue = unsafe { device.get_device_queue(present_index, 0) };
        (device, compute_queue, present_queue)
    }

    pub fn new_swapchain(
        vk_context: &VkContext,
        queue_families_indices: QueueFamiliesIndices,
        extent: vk::Extent2D,
    ) -> (
        Swapchain,
        vk::SwapchainKHR,
        SwapchainProperties,
        Vec<vk::Image>,
    ) {
        let details = unsafe {
            SwapchainSupportDetails::new(
                vk_context.physical_device(),
                vk_context.surface(),
                vk_context.surface_khr(),
            )
        };
        let usage = details.capabilities.supported_usage_flags;
        if !usage.contains(vk::ImageUsageFlags::STORAGE) {
            panic!("Swapchain doesn't support VK_IMAGE_USAGE_STORAGE BIT!");
        }
        let properties = details.get_ideal_swapchain_properties(extent);
        log::debug!(
            "Preferred swapchain extent: {:?}. Actual: {:?}",
            extent,
            properties.extent
        );
        let SwapchainProperties {
            format,
            present_mode,
            extent,
        } = properties;
        let image_count = {
            let max = details.capabilities.max_image_count;
            let preferred = details.capabilities.min_image_count + 1;
            if 0 < max && max < preferred {
                max
            } else {
                preferred
            }
        };

        log::debug!(
            "Creating swapchain.\n\tFormat: {:?}\n\tColorSpace: {:?}\n\tPresentMode: {:?}\n\tExtent: {:?}\n\tImageCount: {:?}",
            format.format,
            format.color_space,
            present_mode,
            extent,
            image_count,
        );

        let QueueFamiliesIndices {
            compute_index,
            present_index,
        } = queue_families_indices;
        let family_indices = [compute_index, present_index];

        let create_info = {
            let mut builder = vk::SwapchainCreateInfoKHR::builder()
                .surface(vk_context.surface_khr())
                .min_image_count(image_count)
                .image_format(format.format)
                .image_color_space(format.color_space)
                .image_extent(extent)
                .image_array_layers(1)
                .image_usage(vk::ImageUsageFlags::STORAGE);

            builder = if compute_index != present_index {
                builder
                    .image_sharing_mode(vk::SharingMode::CONCURRENT)
                    .queue_family_indices(&family_indices)
            } else {
                builder.image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            };

            builder
                .pre_transform(details.capabilities.current_transform)
                .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
                .present_mode(present_mode)
                .clipped(true)
                .build()
        };

        let swapchain = Swapchain::new(vk_context.instance(), vk_context.device());
        let swapchain_khr = unsafe { swapchain.create_swapchain(&create_info, None) }.unwrap();
        let images = unsafe { swapchain.get_swapchain_images(swapchain_khr) }.unwrap();
        (swapchain, swapchain_khr, properties, images)
    }

    fn new_image_view(
        device: &Device,
        &image: &vk::Image,
        format: vk::Format,
        aspect_mask: vk::ImageAspectFlags,
    ) -> vk::ImageView {
        let create_info = vk::ImageViewCreateInfo::builder()
            .image(image)
            .view_type(vk::ImageViewType::TYPE_2D)
            .format(format)
            .subresource_range(vk::ImageSubresourceRange {
                aspect_mask,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            })
            .build();
        unsafe { device.create_image_view(&create_info, None) }.unwrap()
    }

    pub fn new_image_views(
        device: &Device,
        swapchain_images: &[vk::Image],
        swapchain_properties: SwapchainProperties,
    ) -> Vec<vk::ImageView> {
        let to_img_view = |image: &vk::Image| {
            Self::new_image_view(
                device,
                image,
                swapchain_properties.format.format,
                vk::ImageAspectFlags::COLOR,
            )
        };
        swapchain_images.iter().map(to_img_view).collect()
    }

    pub fn new_command_pool(
        device: &Device,
        compute_index: u32,
        create_flags: vk::CommandPoolCreateFlags,
    ) -> vk::CommandPool {
        let command_pool_info = vk::CommandPoolCreateInfo::builder()
            .queue_family_index(compute_index)
            .flags(create_flags)
            .build();
        unsafe { device.create_command_pool(&command_pool_info, None) }.unwrap()
    }

    fn execute_one_time_commands<F: FnOnce(vk::CommandBuffer)>(
        device: &Device,
        command_pool: vk::CommandPool,
        queue: vk::Queue,
        executor: F,
    ) {
        let command_buffer = {
            let alloc_info = vk::CommandBufferAllocateInfo::builder()
                .level(vk::CommandBufferLevel::PRIMARY)
                .command_pool(command_pool)
                .command_buffer_count(1)
                .build();
            unsafe { device.allocate_command_buffers(&alloc_info).unwrap()[0] }
        };
        {
            let begin_info = vk::CommandBufferBeginInfo::builder()
                .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT)
                .build();
            unsafe { device.begin_command_buffer(command_buffer, &begin_info) }.unwrap();
        }
        executor(command_buffer);
        unsafe { device.end_command_buffer(command_buffer) }.unwrap();
        {
            let submit_info = vk::SubmitInfo::builder()
                .command_buffers(&[command_buffer])
                .build();
            unsafe {
                device
                    .queue_submit(queue, &[submit_info], vk::Fence::null())
                    .unwrap();
                device.queue_wait_idle(queue).unwrap();
            };
        }
        unsafe { device.free_command_buffers(command_pool, &[command_buffer]) };
    }

    pub fn new_dev_loc_buf_with_data<T: Copy>(
        (vk_context, command_pool, transfer_queue): (&VkContext, vk::CommandPool, vk::Queue),
        usage: vk::BufferUsageFlags,
        data: &[T],
    ) -> Buffer {
        let device = vk_context.device();
        let size = (size_of::<T>() * data.len()) as _;
        const MASK: vk::MemoryMapFlags = vk::MemoryMapFlags::empty();
        let mut buf = unsafe { vk_context.new_staging_buf(size) };
        unsafe {
            let ptr = device.map_memory(buf.memory, 0, size, MASK).unwrap();
            Align::new(ptr, align_of::<T>() as _, size).copy_from_slice(&data);
            device.unmap_memory(buf.memory);
        }
        let dev_buf = unsafe { vk_context.new_dev_loc_buf(buf.size, usage) };
        let regions = &[vk::BufferCopy {
            src_offset: 0,
            dst_offset: 0,
            size,
        }];
        Self::execute_one_time_commands(&device, command_pool, transfer_queue, |buffer| unsafe {
            device.cmd_copy_buffer(buffer, buf.buffer, dev_buf.buffer, regions)
        });
        unsafe { buf.drop(device) };
        dev_buf
    }

    pub fn stage_hash_dag_changes(&mut self, dag: &mut BasicHashDAG) {
        let specs = dag.staging_specs();
        if 0 < specs.total_size() {
            if 10_000_000 < specs.total_size() {
                log::warn!("Staging {} bytes of HashDAG data...", specs.total_size());
            } else {
                log::debug!("Staging {} bytes of HashDAG data...", specs.total_size());
            }

            let time = SystemTime::now();
            let device = self.vk_context.device();
            let (pool, lut) = dag.dump();
            const MASK: vk::MemoryMapFlags = vk::MemoryMapFlags::empty();

            let (mut pool_src_buf, mut lut_src_buf) = unsafe {
                (
                    self.vk_context.new_staging_buf(specs.pool_size() as _),
                    self.vk_context
                        .new_staging_buf(specs.page_table_size() as _),
                )
            };
            let mut pool_regions = Vec::with_capacity(1 << 14);
            let mut lut_regions = Vec::with_capacity(8);

            unsafe {
                // ash::util::Align was a bit too restrictive. Please be careful.
                let map = |buf: &Buffer| device.map_memory(buf.memory, 0, buf.size, MASK);
                let (pool_ptr, lut_ptr) = (map(&pool_src_buf).unwrap(), map(&lut_src_buf).unwrap());
                let pool_mapped = from_raw_parts_mut(pool_ptr as *mut u32, specs.pool_items as _);
                let lut_mapped = from_raw_parts_mut(lut_ptr as *mut u32, specs.pages as _);
                let to_buffer_copy = |src_range: &Range<_>, dst_range: &Range<_>| vk::BufferCopy {
                    src_offset: (src_range.start * size_of::<u32>()) as _,
                    dst_offset: (dst_range.start * size_of::<u32>()) as _,
                    size: (dst_range.len() * size_of::<u32>()) as _,
                };
                dag.stage(
                    |src_range, dst_range| {
                        pool_regions.push(to_buffer_copy(&src_range, &dst_range));
                        pool_mapped[src_range].copy_from_slice(&pool[dst_range]);
                    },
                    |src_range, dst_range| {
                        lut_regions.push(to_buffer_copy(&src_range, &dst_range));
                        lut_mapped[src_range].copy_from_slice(&lut[dst_range]);
                    },
                );
                dag.tracker.clear();
                device.unmap_memory(pool_src_buf.memory);
                device.unmap_memory(lut_src_buf.memory);
            }

            let (pool_src, lut_src) = (pool_src_buf.buffer, lut_src_buf.buffer);
            let (pool_dst, lut_dst) = (self.pool_buf.buffer, self.page_table_buf.buffer);
            let exec = move |cmd_buf| unsafe {
                device.cmd_copy_buffer(cmd_buf, pool_src, pool_dst, &pool_regions);
                device.cmd_copy_buffer(cmd_buf, lut_src, lut_dst, &lut_regions);
            };
            Self::execute_one_time_commands(device, self.command_pool, self.compute_queue, exec);
            unsafe {
                pool_src_buf.drop(device);
                lut_src_buf.drop(device);
            }

            let dt = SystemTime::now().duration_since(time).unwrap().as_millis();
            if 10 < dt {
                log::warn!("Spent {}ms on staging.", dt);
            } else {
                log::debug!("Spent {}ms on staging.", dt);
            }
        }
    }

    pub fn create_descriptor_pool(device: &Device) -> vk::DescriptorPool {
        let pool_info = vk::DescriptorPoolCreateInfo::builder()
            .pool_sizes(&[
                vk::DescriptorPoolSize {
                    ty: vk::DescriptorType::STORAGE_IMAGE,
                    descriptor_count: 1,
                },
                vk::DescriptorPoolSize {
                    ty: vk::DescriptorType::STORAGE_BUFFER,
                    descriptor_count: 3,
                },
                vk::DescriptorPoolSize {
                    ty: vk::DescriptorType::UNIFORM_BUFFER,
                    descriptor_count: 1,
                },
            ])
            .max_sets(1)
            .build();
        let pool = unsafe { device.create_descriptor_pool(&pool_info, None).unwrap() };
        log::debug!("Created a descriptor pool.");
        pool
    }

    fn read_shader_from_file<P: AsRef<std::path::Path>>(path: P) -> Vec<u32> {
        fn load<P: AsRef<Path>>(path: P) -> Cursor<Vec<u8>> {
            let mut buf = Vec::new();
            let full_path = &Path::new("assets").join(&path);
            let mut file = File::open(&full_path).unwrap();
            file.read_to_end(&mut buf).unwrap();
            Cursor::new(buf)
        }

        log::debug!("Loading shader file {}", path.as_ref().to_str().unwrap());
        let mut cursor = load(path);
        read_spv(&mut cursor).unwrap()
    }

    fn create_shader_module(device: &Device, code: &[u32]) -> vk::ShaderModule {
        let create_info = vk::ShaderModuleCreateInfo::builder().code(code).build();
        unsafe { device.create_shader_module(&create_info, None).unwrap() }
    }

    pub fn new_pipeline<DAG: HashDAG, T: Tracker>(
        device: &Device,
        descriptor_set_layout: vk::DescriptorSetLayout,
        dag: &SharedHashDAG<DAG, T>,
        (extent, fov, aperture): (vk::Extent2D, f32, f32),
    ) -> (vk::Pipeline, vk::PipelineLayout) {
        let trace_source = Self::read_shader_from_file("shaders/ray_tracing.comp.spv");
        let trace_shader_module = Self::create_shader_module(device, &trace_source);

        let (data, map_entries) = {
            const CONSTS_DATA: [[u8; 4]; 6] = [
                u32::to_le_bytes(LOCAL[0]),
                u32::to_le_bytes(LOCAL[1]),
                u32::to_le_bytes(SUPPORTED_LEVELS),
                u32::to_le_bytes(LEAF_LEVEL),
                u32::to_le_bytes(PAGE_LEN),
                u32::to_le_bytes(TOTAL_PAGES),
            ];
            let mut map_entries = data_to_map_entries!(0, 0, CONSTS_DATA);
            let data = {
                let height = extent.height as f32;
                let [width_rcp, height_rcp] = [1.0 / extent.width as f32, 1.0 / height];
                let dx = fov.tan();
                let dy = -dx * height * width_rcp;
                log::info!("Changing viewport to: {:?}", extent);
                let data = [
                    u32::to_le_bytes(dag.dump().0.len() as _),
                    f32::to_le_bytes(width_rcp),
                    f32::to_le_bytes(height_rcp),
                    f32::to_le_bytes(dx),
                    f32::to_le_bytes(dy),
                    f32::to_le_bytes(aperture),
                ];

                let last_entry = map_entries[map_entries.len() - 1];
                let start_offset = last_entry.offset + last_entry.size as u32;
                map_entries.extend(data_to_map_entries!(10, start_offset, data));
                [CONSTS_DATA.concat(), data.concat()].concat()
            };
            (data, map_entries)
        };

        let layout = {
            let layout_info = vk::PipelineLayoutCreateInfo::builder()
                .set_layouts(&[descriptor_set_layout])
                .push_constant_ranges(&[vk::PushConstantRange {
                    stage_flags: vk::ShaderStageFlags::COMPUTE,
                    offset: 0,
                    size: size_of::<crate::trace::PushConstants>() as _,
                }])
                .build();

            unsafe { device.create_pipeline_layout(&layout_info, None).unwrap() }
        };

        let entry_point_name = CString::new("main").unwrap();
        let pipeline_info = {
            let specialization_info = vk::SpecializationInfo::builder()
                .map_entries(&map_entries)
                .data(&data)
                .build();
            let trace_shader_state_info = vk::PipelineShaderStageCreateInfo::builder()
                .stage(vk::ShaderStageFlags::COMPUTE)
                .module(trace_shader_module)
                .name(&entry_point_name)
                .specialization_info(&specialization_info)
                .build();
            vk::ComputePipelineCreateInfo::builder()
                .stage(trace_shader_state_info)
                .layout(layout)
                .build()
        };

        let pipeline = unsafe {
            device.create_compute_pipelines(vk::PipelineCache::null(), &[pipeline_info], None)
        }
        .unwrap()[0];

        unsafe { device.destroy_shader_module(trace_shader_module, None) };

        (pipeline, layout)
    }

    pub fn drop_swapchain(&mut self) {
        let device = self.vk_context.device();
        unsafe {
            device.free_command_buffers(self.command_pool, &self.command_buffers);
            device.destroy_pipeline(self.pipeline, None);
            device.destroy_pipeline_layout(self.pipeline_layout, None);
            for &image_view in self.swapchain_image_views.iter() {
                device.destroy_image_view(image_view, None);
            }
            self.swapchain.destroy_swapchain(self.swapchain_khr, None);
        }
    }

    #[inline]
    pub fn wait_gpu_idle(&mut self) {
        unsafe { self.vk_context.device().device_wait_idle() }.unwrap();
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        log::debug!("Dropping application. Waiting for operations to finish...");
        {
            let device = self.vk_context.device();
            match unsafe { device.wait_for_fences(&[self.fence], true, 500_000_000) } {
                Err(vk::Result::TIMEOUT) => {
                    log::debug!("Timeout. Forcing the application to finish.")
                }
                _ => log::debug!("Operations finished."),
            }
        }
        self.drop_swapchain();
        let device = self.vk_context.device();
        unsafe {
            device.destroy_descriptor_pool(self.descriptor_pool, None);
            device.destroy_descriptor_set_layout(self.descriptor_set_layout, None);

            self.pool_buf.drop(device);
            self.page_table_buf.drop(device);
            self.static_buf.drop(device);
            self.model_buf.drop(device);

            device.destroy_semaphore(self.image_available, None);
            device.destroy_semaphore(self.render_finished, None);
            device.destroy_fence(self.fence, None);

            device.destroy_command_pool(self.command_pool, None);
        }
    }
}

#[derive(Clone, Copy)]
pub struct QueueFamiliesIndices {
    pub compute_index: u32,
    pub present_index: u32,
}
