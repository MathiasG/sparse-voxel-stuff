# Sparse Voxel Stuff

**Project's "dead" (nothing like this anymore & is frozen)**

Ultimately, this project is intended more as an inspiration than a plug-and-play solution as this will be the groundwork for my future project(s). Specifically the only project that is really set in stone is a game (supporting multiplayer). This means I'll probably optimize whatever I learned and concocted to both the GPU and CPU (client and server).

I would also like to note that not much is really defined as of yet. I don't know really what the potential is and I hope to find out soon, but I do have the philosophy to keep things simple. I think that basically everyone is new to this and that there is a massive amount of golden nuggets to be unearthed. That and me having the goal to shift my efforts to my envisioned game in a couple years.

My expectations are that this will not get a lot of attention (at least at first), but I'd like this to be a contribution to anyone who is thinking about using the same technology I now tinker with. I hope that this could help put the technology to the test in commercial gaming and mark a beginning along with all other recent efforts and advancements made in researching this topic.

Finally, I'm developing on a private git repo and I push to this only when I reach a certain milestone. It's not perfect, but I plan to change that. I just don't feel to confident as I tend to rewrite and refactor things beyond recognition _a lot_ (which probably explains why progress is soo slow). It leads me to conclude it's probably better to do that behind closed doors to prevent confusion. I hope for this to change when I'm a bit more experienced. *That means however that things may look somewhat dead. Just poke me if you're not too sure (mathias.grosse@outlook.be).*

## What is this?

No matter how technically inclined you are and wether you're in the know or not I'd recommend visiting <https://newq.net/>. On thing it has is a Eurographics 2018 tutorial which has introduction slides relevant to this. If you want to dig deeper there is a lot of material just in these slides, but not enough to actually be able to *do* anything other than talk about it. If you want to learn more you can chase the links.

The slides are great, but they are from 2018 and in the meantime a paper came out that introduced the HashDAG (which this project uses). The HashDAG paper can be found on the same site under publications. The paper is called "Interactively Modifying Compressed Sparse Voxel Representations".

All-together it may seem complex, but it's based on rather simple concepts and is really elegant. Of course a lot of simple can be a tad hard so basically it's like math but **objectively more fun**.

![Screenshot of the application in October of 2020](Screenshot_2020-10-18_13-53-34.png "A view of the Epic Citadel SVDAG")

## Instructions

All you need to do is make sure you have rustup installed and then you build & run using this command (in the workspace directory): ```cargo run --release```.

The building process should not require anything more, except for when you're on Windows and are new to Rust. In this case you can run it anyway and you'll get instructions on how to get msvc. For people who don't like that I'd recommend setting Rust up through rustup (toolchains) to use a different compiler.

If you don't want to run it after build run: ```cargo build --release```.

You can of course also run these commands without the release flag. When you run it in release mode the binary is optimized and both (RenderDoc & Khronos) validation layers are disabled. However you should be aware of the fact that optimizations do make a very real difference.

### Logging

If something goes wrong anywhere in the application it is not always guaranteed to be caught by the logger. If this is the case it makes sense to change the log-level to help trace the cause. You can do this through the environment variable `RUST_LOG`. Applicable log-levels are trace, debug, info, warn and error.

**Note:** Unrecoverable errors (which are called panics in Rust) are not logged. Instead they are displayed in the terminal. So if the demo simply closes without the cause for it being logged, you should look for a cause in the terminal.

### Dependency: Vulkan

The default binary depends on Vulkan which means that any non-compatible HW won't be able to build nor run it. However, it is possible to build the application by disabling shader compilation through the environment variable `SKIP_SHADER_COMPILATION`. Of course running the default binary still isn't possible but you can run other binaries which do not require Vulkan.

### Dependency: RenderDoc

At the moment the default demo looks for a RenderDoc module. However, having RenderDoc installed is not a hard requirement.

If you'd like to "pair it" with RD you'll need to have its library file linked or copied to the workspace directory.
- In Windows it is named "C:/Program Files/RenderDoc/renderdoc.dll"
- In Linux it is named "/usr/lib/librenderdoc.so"

Having this set up and then launching the application in RenderDoc does not conflict.

### Tests

Say you want to dig in more and run some tests. Then all you should now is that certain tests expect a 128k Epic Citadel SVDAG to be in ".local/svdags". You can get it [here](https://drive.google.com/drive/folders/1P3T_wlC3vOcxQSKh9QD0WtB3yNlbPLV2). It is not a hard requirement, but some tests will fail.

## To any potential contributors

I'd *love* to see people contribute to this as it is quite a big undertaking, however much I'm prepared to do this alone.

### So what if you're interested in contributing?

If it's just a correction or missing [documentation](#documentation), I'd say go right ahead. If it is something possibly more controversial or you're not certain, I'd say try me first. If our views don't align you can always fork it. To me that's a contribution anyways as it is one of my goals to make this technology more accessible (else I wouldn't open-source this).

### In case you're new to Rust

A point I really want to get across is that using unsafe is making a promise. Trying to avoid it is leveraging on previous work that you'd (mostly) have to reason through if you were to use unsafe anyway, which saves everyone's time. One can not always avoid it however. It *can* result in a loss of performance and sometimes a loss too great. In this case you'll just have to use unsafe which is essentially trading in development time for performance, promising what's unsafe to the compiler is not actually unsafe to anyone using your code. **Which is why it's so important in Rust.**

## It's heavily inspired by ...

I'd say I got most milage out of a standalone ray tracer that was supposed to be included in a **[Eurographics 2018 tutorial](https://newq.net/publications/more/eg2018-voxel-dag-tutorial)**. It almost felt like I reverse-engineered it and I grew to understand some very clever concepts I'd otherwise never have picked up on. (I'm not sure if it's actually online, but Phyronnaz/HashDAG has the same ray-tracer.)

After that, when the paper introducing the HashDAG came out another very valuable resource became **[Phyronnaz/HashDAG](https://github.com/Phyronnaz/HashDAG)**. This too is a very important resource to this project. My experience learning was completely different this time. I always first tried something myself and when it took too long or when I was done I peeked at the solution like it's a cheat-sheet. That I could do that only made me appreciate the help I got earlier even more. *I should also mention that as of today I'm still not done. I'm exploring some concepts not covered in this project, but it's the only implementation of color-trees available at the moment (that I know of).*

## Sample models

I included a lantern SVDAG inside the repository to make things more convenient, but there are other SVDAGs one can try (more hi-res ones) at <https://drive.google.com/drive/folders/1P3T_wlC3vOcxQSKh9QD0WtB3yNlbPLV2>.

The original lantern model can be found [here](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/Lantern).

## Documentation

Inline documentation never touches on the voxel technology itself and only on implementation specifics and I have not written and do not plan to write any other documentation. I'd like it to be here, but I'm more willing to invest my time elsewhere. That doesn't mean however that any contributions involving this wouldn't be appreciated. However it does mean that if documentation about implementation specifics is vague or simply missing I won't hold you against making an issue for it.

Aside from all that, if you're someone who wrote something about SVDAGs, HashDAGs or anything seemingly relevant to this work I'm more than happy to reference it here (assuming it's correct).

## What is done

An implicit milestone involved everything **modifiable geometry** (except for concurrency) with items such as:
- Importing SVDAGs
- Track what changed on page-level (did node-tracking too, but it was not done "the best way" and hinted at slower staging)
- Staging only what changed (all at once)
- Live editing with different primitives
- Ray tracer (with aperture, but no "real" cone-tracing, as textures are not yet supported)

## The plan

There is no real plan. I've noticed I tend to work better if I only keep record of the technical details (2 weeks after settling on it). At most what I keep are hints to nudge me back on the same train of thought. It just doesn't seem really advantageous to go in detail when you still have to discover what can be done and how it is done.

**However:** Even though I have no real plans and not even real requirements (as of yet) I do have a very specific goal and that is to figure out what I can do. The first project I have in mind after this is to make a game with its closest equivalent being StarMade. Though, no matter how different the back-end technology would end up being, it should look nothing like it. I have purposefully set quite vague goals just because I don't really know what to expect. This way I should be able to go ahead no matter how disappointing or exhilarating the solution ends up being. As long as it is "finished".

This means I *do* have a view on what tech I'd like, here is a non-exhaustive list:
- Analytical tools
- Lossless and lossy attribute compression algorithms
- Anisotropic mip-mapping
- Voxel cone-tracing
- Real-time streaming
- Render multiple spatially independent DAGs at once
- Allocator that permits dynamically purging nodes
- Concurrency
- Exporting
- (Probably) Lookup optimization (with as options):
  - General optimization
  - A manually or algorithmically maintained LRU table (like the full node table)
- (Probably) More advanced conversion allowing clever importing and exporting
  - Iterative importing optimization allowing in-memory LOD
  - Recognizing/recording re-used SVDAGs (in separate imports) allowing to save much more disk-space and speeding up both imports and exports
- (Maybe) Many views
- (Maybe) An editor optimized to a very specific use-case
- (Maybe) Exploit frame-to-frame coherency

Some of these ideas can get pretty wild so I won't get to implement them all and not to their full extent, but this is only what I'd like, not necessarily what I can afford. I won't work in just any arbitrary order either, I'll make sure to prioritize, so as long as I get the vital ones crossed of (vital to the next project) I will be satisfied.

## What needs some tinkering still

Of course almost everything does, but to clarify: I mean things that should have been already, but I didn't manage it so far. Probably because I just didn't know how to deal with it.

**Graphics.** That's a big one, for at the moment it is almost everything. Something I can immediately point out are the very detailed and specific errors I get from RenderDoc. I also have floating point issues. Should I also mention I "learned" Vulkan through this project? There are probably some optimizations to be had.

Please keep this in mind if either you want to use snippets, fork or contribute.

## Project history

Old to new:
- From March trough May '20: I built my very first **SVDAG ray-tracer** which allowed ray-tracing a single oriented SVDAG with colors.
- From June to mid-July '20: I went from reading the modifiable compressed voxel paper to **implementing a HashDAG (geometry only)** and importing a small SVDAG and rendering it. I then tried making sense of color trees by copying the C++ implementation, but changed my mind halfway to attend to other things first.
- From mid-July through August '20: **Transitioned from the PoC**, including editing (still no colors).
- Through entire September '20: **Moved from Vulkano to Ash** as I was constantly struggling with the API and Ash seemed much simpler. I still had a lot of the Vulkan-API ahead of me so I'd call it justified. (Must say this: I wasn't wrong about Ash, but it's like moving from writing IKEA manuals to writing the Bible. It was soo boring. That's probably why it took so long.)
- From October to mid-October '20: **Implemented both tracking and staging.** I tried multiple versions of change-tracking and staging. I finally settled on the simplest one I had in mind as it also happened to be the fastest.

## The focus right now

The focus will now be on enabling IPC through shared memory after which I will script some analytical tools, most likely in Python. I also have a new allocator planned to allow for dynamically purging nodes and lookup optimization which can apply on both the version with the new allocator as without. What will stick however only time will tell, but I will update this repo the moment I have a hit or miss on a feature. I will also get to attributes some day, but most likely I'll get these ideas implemented first.

## About the maintainer

I basically don't know much (yet). I'm still junior in everything at best and am learning as I go. All my experience before has been limited to .NET, Python, Java and such. Nothing low-level. I also have almost no experience in game development and neither in computer graphics. I always shied away from game development because it seemed like I wasn't cut out for it and computer graphics didn't really exist in my world.

I have soo much relevant experience I can list it!.. But I should not make this a shit-post about myself (I admit I had it *all* written down, until I realized I also put this in my resumé). So instead -- now that I incidentally also announced I'm a job-seeker -- I'll mention that last couple of months are not representative of what's to come.

I guess it'd be pretty stupid not to include any contact information... Or not to mention my name or any alias...
