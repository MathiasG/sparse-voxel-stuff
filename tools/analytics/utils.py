from analytics.constants import PAGE_TABLE, FREE_STORE, DATA_POOL, CONFIG, HASH_DAG

import sys
from os.path import normpath, sep
import os
import mmap
import multiprocessing.shared_memory as sm
import numpy as np
import json


def fail(msg='The maintainer of this tool was too lazy to specify what failed.'):
    '''Handy for unrecoverable failures. May want to supply a message.'''
    print(f'Failed. {msg}')
    sys.exit(1)


def read_once(path):
    '''Must exist already, but I'm rusty so don't know what to invoke.'''
    try:
        with open(path, 'r') as read_once:
            return read_once.read()
    except FileNotFoundError:
        fail(f'{path} does not exist.')


def load_shmem(shm_file, field, dtype):
    '''Loads a single field whilst making some assumptions. Load into numpy array.'''
    # Ideally Python wouldn't need a workaround, but the Linux implementation is buggy
    if os.name == 'posix':
        fd = open(f'/dev/shm{shm_file}', 'r+b')
        shmem = mmap.mmap(fd.fileno(), 0)
        array = np.frombuffer(shmem, dtype)
    else:
        shmem = sm.SharedMemory(shm_file)
        array = np.frombuffer(shmem.buf, dtype)
    return shmem, array


def load_hash_dag(root, name):
    '''Loads a complete HashDAG from shmem, which includes: the pool, LUT, free-store & name.'''
    def load_shmem_inner(data, field, dtype):
        shm_file = read_once(normpath(f'{root}-{field}.flink'))
        data['shmem'][field], data[field] = load_shmem(shm_file, field, dtype)

    data = {'shmem': {}, 'name': name}
    load_shmem_inner(data, PAGE_TABLE, np.uint32)
    load_shmem_inner(data, FREE_STORE, np.uint32)
    load_shmem_inner(data, DATA_POOL, np.uint32)
    return data


def load_state(path, name):
    '''Making assumptions of the setup, this loads a configuration & HashDAG from a shmem-store & name.'''
    config = json.loads(read_once(normpath(f'{path}/{name}-config.json')))
    hash_dag = load_hash_dag(normpath(f'{path}/{name}'), name)
    return {CONFIG: config, HASH_DAG: hash_dag}


def collect_stats(config, hash_dag):
    '''Very generic, making no assumptions, yet interesting.'''
    stats = {
        'pool_bytes': len(hash_dag[DATA_POOL].tobytes()),
        'relative_size': hash_dag[DATA_POOL].size / config['TOTAL_VIRT_SPACE']
    }
    stats['bytes_total'] = stats['pool_bytes'] + \
        len(hash_dag[FREE_STORE].tobytes()) + \
        len(hash_dag[PAGE_TABLE].tobytes())
    return stats


def file_links(files):
    '''
    Gets all recognizable file links from a list of file names.
    Returns a map by field-name returning file-name.
    '''
    flink_suffixes = [
        f'-{PAGE_TABLE}.flink',
        f'-{FREE_STORE}.flink',
        f'-{DATA_POOL}.flink'
    ]
    grouped = {}
    for i in files:
        if not any(i.endswith(s) for s in flink_suffixes):
            continue
        name = i[i.rfind(sep) + 1:i.rfind('-')]
        if name in grouped:
            grouped[name].append(i)
        else:
            grouped[name] = [i]
    return grouped


def is_valid_flink(flink):
    '''Tries a file link by mapping the linked data.'''
    shm_file = read_once(flink)
    try:
        flink = flink[flink.rfind('-')+1:-len('.flink')]
        load_shmem(shm_file, flink, np.uint8)
    except:
        return False
    return True


def is_valid_hash_dag(file_links):
    '''Can read directly from `file_links(..)`. Returns a map of True/False with DAG-names.'''
    validations = {True: [], False: []}
    for name in file_links:
        if len(file_links[name]) != 3:
            fail('A HashDAG has too little or too many file links.')
        is_valid = all(is_valid_flink(flink) for flink in file_links[name])
        validations[is_valid].append(name)
    return validations
