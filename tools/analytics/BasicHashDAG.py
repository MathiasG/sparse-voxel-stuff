from analytics.HashDAG import new_bucket_len_idx, hi_lo_levels, new_bucket_len, buckets_per_level
from analytics.constants import FREE_STORE

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np


def plot_live(fig, update_fig, dag_name, interval):
    '''Helper for animated plotting by interval.'''
    fig.suptitle(f'Live stats of "{dag_name}"', fontsize=16)
    update_fig(None)
    _ = FuncAnimation(fig, update_fig, interval=interval)
    plt.show()


def allocated_pages_stack(config, hash_dag, interval=1000, **_):
    '''Plot (stack-plot) how many pages are allocated per level.'''
    fig, ax = plt.subplots(1, 1, figsize=(80, 60), constrained_layout=True)
    hi_levels, lo_levels = hi_lo_levels(**config)
    x, y, labels = [], [], []

    for level in range(lo_levels[-1]+1):
        y.append([])
        labels.append(f'Level {level}')

    def update_fig(frame):
        initialization = frame is None

        def update_data():
            local_y = []

            def inner(levels):
                l0 = levels[0]
                lo_idx = new_bucket_len_idx(l0, **config)
                for level in levels:
                    hi_idx = new_bucket_len_idx(level + 1, **config)
                    local_y.append(sum(hash_dag[FREE_STORE][lo_idx:hi_idx]))
                    lo_idx = hi_idx

            inner(hi_levels)
            inner(lo_levels)

            def append_local():
                for lvl, pages in enumerate(local_y):
                    y[lvl].append(pages)

            is_diff = initialization
            if not initialization:
                is_diff = any(
                    y[lvl][-1] != pages
                    for lvl, pages in enumerate(local_y)
                )
            if is_diff:
                append_local()
            return is_diff

        def update_stack():
            local_labels = []
            total = sum(y[lvl][-1] for lvl in range(len(labels)))
            for lvl, label in enumerate(labels):
                pcnt = round(y[lvl][-1] * 100 / total, 2)
                local_labels.append(f'{label} ({pcnt}%)')
            ax.cla()
            ax.stackplot(x, y, labels=local_labels)
            ax.set_ylabel('Allocated pages')
            ax.set_xlabel('Timestep')
            ax.set_title('Allocated pages per level')
            ax.legend(loc='upper left')

        if update_data():
            x.append(0 if initialization else x[-1] + 1)
            update_stack()

    plot_live(fig, update_fig, hash_dag['name'], interval)


def bucket_occupancy_bar(config, hash_dag, interval=1000, binning=np.max, bins=15, sharey=False, **_):
    '''Plot (bar-plot) how buckets are occupied.'''
    fig, ax = plt.subplots(
        2, 1, figsize=(80, 60), sharey=sharey, constrained_layout=True
    )
    hi_levels, lo_levels = hi_lo_levels(**config)
    BUCKETS_PER_HI_LEVEL = config['BUCKETS_PER_HI_LEVEL']
    BUCKETS_PER_LO_LEVEL = config['BUCKETS_PER_LO_LEVEL']
    hi_bin_size, lo_bin_size = BUCKETS_PER_HI_LEVEL // bins, BUCKETS_PER_LO_LEVEL // bins
    hi_title = f'Bucket occupancy of high levels (binned using {binning.__name__})'
    lo_title = f'Bucket occupancy of low levels (binned using {binning.__name__})'
    hi_max_len = new_bucket_len(hi_levels[0], **config)
    lo_max_len = new_bucket_len(lo_levels[0], **config)
    x_data, y_data = np.arange(bins), {}

    def update_fig(_):
        def update_data():
            def inner(levels, bin_size):
                l0 = levels[0]
                lo_idx = new_bucket_len_idx(l0, **config)
                bpl = buckets_per_level(l0, **config)
                for level in levels:
                    hi_idx = new_bucket_len_idx(level + 1, **config)
                    lvl_free_store = hash_dag[FREE_STORE][lo_idx:hi_idx]
                    y_data[level] = [
                        binning(lvl_free_store[lo*bin_size:(lo+1)*bin_size]) for lo in x_data[:-1]
                    ]
                    # The amount of buckets may not be divisible by bin_size so include anything left in the last bin.
                    lo = x_data[-1] * bin_size
                    y_data[level].append(binning(lvl_free_store[lo:bpl]))
                    lo_idx = hi_idx

            inner(hi_levels, hi_bin_size)
            inner(lo_levels, lo_bin_size)

        def update_bar(levels, max_len, bin_size, ax, title, total_width=.95):
            l0 = levels[0]
            ax.cla()
            width, off = total_width / len(levels), l0 + (levels[-1] - l0) / 2
            for level in levels:
                x, y = x_data + width * (level - off), y_data[level]
                ax.bar(x, y, width=width, label=f'Level {level}')
            ax.set_ylabel(f'Occupancy (max. {max_len})')
            ax.set_xlabel('Bucket')
            ax.set_title(title)
            ax.legend(loc='upper left')
            ax.set_xticks(x_data)
            ax.tick_params(labelsize=8)
            labels = [f'{i*bin_size}-{(i+1)*bin_size}' for i in x_data[:-1]]
            buckets = buckets_per_level(l0, **config)
            labels.append(f'{x_data[-1]*bin_size}-{buckets}')
            ax.set_xticklabels(labels)

        update_data()
        update_bar(hi_levels, hi_max_len, hi_bin_size, ax[0], hi_title)
        update_bar(lo_levels, lo_max_len, lo_bin_size, ax[1], lo_title)

    plot_live(fig, update_fig, hash_dag['name'], interval)
