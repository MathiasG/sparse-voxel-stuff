#!/usr/bin/python3

from analytics.constants import SHMEM_STORE
from analytics.utils import is_valid_hash_dag, file_links

from os import listdir, remove
from os.path import normpath

'''
Meant to clean store on crash, but links are still relevant then.
Would need to map mem & shutdown to close leak & find invalid links.
(Can do that; just didn't do it.)
'''


def main():
    print('Make sure to run this from the project root directory!\n')
    shmem_store_items = listdir(SHMEM_STORE)
    items = [normpath(f'{SHMEM_STORE}/{item}')
             for item in shmem_store_items]
    invalids = is_valid_hash_dag(file_links(items))[False]
    print(f'Removing {len(invalids)} invalid HashDAG(s)...')
    for item in shmem_store_items:
        for invalid in invalids:
            prefix = f'{invalid}-'
            is_full_name = item.rfind('-') == prefix.rfind('-')
            if item.startswith(prefix) and is_full_name:
                remove(normpath(f'{SHMEM_STORE}/{item}'))
    print('Done!')


if __name__ == '__main__':
    main()
