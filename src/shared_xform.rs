use nalgebra::{Isometry3, Matrix4, RealField, SimdRealField, UnitQuaternion, Vector3};
use std::ops::{Deref, DerefMut};

pub type Target = f32;

#[derive(Debug)]
pub struct SharedTransform<N: SimdRealField = Target> {
    inner: Isometry3<N>,
    matrix: Matrix4<N>,
    pub(crate) changed: bool,
}

impl<N: SimdRealField + RealField> SharedTransform<N> {
    #[inline]
    pub fn update(&mut self) {
        if self.changed {
            self.matrix = self.inner.to_homogeneous();
            self.changed = false;
        }
    }
    #[inline]
    pub fn updated_matrix(&mut self) -> &Matrix4<N> {
        self.update();
        self.matrix().unwrap()
    }
}

impl<N: SimdRealField> SharedTransform<N> {
    #[inline]
    pub fn rot(&self) -> &UnitQuaternion<N> {
        &self.inner.rotation
    }
    #[inline]
    pub fn pos(&self) -> &Vector3<N> {
        &self.inner.translation.vector
    }
    #[inline]
    pub fn matrix(&self) -> Option<&Matrix4<N>> {
        if self.changed {
            None
        } else {
            Some(&self.matrix)
        }
    }
    #[inline]
    pub fn needs_update(&self) -> bool {
        self.changed
    }
}

impl<N: SimdRealField + RealField> From<Isometry3<N>> for SharedTransform<N> {
    #[inline]
    fn from(inner: Isometry3<N>) -> Self {
        Self {
            inner,
            matrix: inner.to_homogeneous(),
            changed: true,
        }
    }
}

impl<N: SimdRealField> Deref for SharedTransform<N> {
    type Target = Isometry3<N>;
    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<N: SimdRealField> DerefMut for SharedTransform<N> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.changed = true;
        &mut self.inner
    }
}
