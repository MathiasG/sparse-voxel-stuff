# All about everything

First I'll advice you to learn the very basics of hash-maps. Preferably skimming over different possible implementations. Maybe the only resource you'll need is [this](https://blog.waffles.space/2018/12/07/deep-dive-into-hashbrown/). Unless you have a short attention span (welcome to the club), reading more about this won't harm.

Second I want you to know, first I explain to myself, then I come back to it and try to explain it to you. It is when you can explain it simply you understand best. Meaning I'm not a teacher and even though I happen to sometimes try, the goal is not to teach you, but myself instead.

## Preliminaries

This document assumes you understand the basic concepts of sparse-voxel compression structures such as SVDAGs. An introduction is missing however so getting it yourself by skimming over the paper discussing modifiable SVDAGs is probably a good idea too.

## Geometry

And here we go.

Know that sometimes things have more nuance, though the concepts always hold true. I try not to dig too much into specifics as to really focus on these concepts.

### Terminology

Some terminology I adopted, some I invented, I sadly even changed one, but after digging more in the graph-theory I thought it was a misnomer so hence why...

Most common / useful terminology in (scoped to) this project:

Name                   | Description
---------------------- |  ------------------------
Interior               | Short for interior-node.
Exterior               | What would be referred to as leaves in many previous papers and in older code.
Voxel                  | Pixel means picture element in case you didn't know, so voxel means...
(SV)DAG                | Could both refer to the structure & an instance in this structure. More vague is graph, more specific is a DAG-instance.
HashDAG                | Specifically a DAG stored on top of a hash-map. I personally try not to refer to instance within it as HashDAGs but rather as DAGs.
Data-pool              | A.k.a. a heap, a.k.a. DP. It contains two things: nodes and space for some more.
Page-table             | A.k.a. a LUT, specific to pagination, a.k.a. PT. It's crucial for managed virtual memory.
Managed virtual memory | A.k.a. MVM. Virtual memory which the application manages. It draws the distinction from OS virtual memory.
Free-store             | Often referred to as FS. It's implementation-specific (hence why not free-list) and tells you where free space is.

The idea of having clear & consistent terminology is sadly only a new idea of mine, so some stuff isn't up to snuff.

### The basics

In essence a HashDAG is just an SVDAG in a (paginated) hash-map (optimized for nodes). So conceptually there are 3 different "units" of memory:
1. Levels
2. Buckets
3. Pages

Why?
1. Levels exist to provide more context. Most likely solutions can exist without it, but levels help in removing constraints in MVM, decreasing the memory-footprint, increasing performance and safety, ... (This is mainly possible by assuming [transitive reduction](https://en.wikipedia.org/wiki/Transitive_reduction).)
2. Buckets are exactly what you'd expect them to be if you are familiar with hash-tables (the way they teach them in schools).
3. Pages are what enables MVM. This allows pay-for-use and thus no attention needs to be payed to growing the DP. It's synonymous to the address space of the DAG.

#### Why pagination

People familiar with hash-maps might be asking why the hash-map is paginated whilst it is possible to grow the hash-map and have even better pay for use. The reason for this is that this is to be used in interactive applications, but specifically there are two reasons for this:
1. Simplicity: It's simpler to use paginated hash-maps than to keep in mind that the hash-map may need to grow / shrink at some point. (The PoC was also a paginated hash-map as it is implemented this way in [here](https://github.com/Phyronnaz/HashDAG).) As it is also to be used in applications which generally have fixed constraints it is OK to hard-code these constraints in MVM.
2. Predictable workloads: Interactive applications are in itself unpredictable so it is more preferable to have predictable workloads than amazing but unpredictable workload to prevent compounding any issues. Something like a resize which *has to* lock writes could potentially cause the application to stutter or hang.

#### Abstraction: memory

There are some concepts one needs to be aware of to understand certain sections in this document which are:
- Read-memory: The part of memory which holds everything necessary to be able to read the sparse-voxel-representation.
- Write-memory: Essentially all other memory which can be used to operate on read-memory (more effectively).
- Lookup-memory: Write-memory which can speed up lookups. This can be tiering or bucket-optimization.
- Allocation-memory: Write-memory which allows for (de)allocation.

##### Example

The vanilla HashDAG which is discussed and implemented in [this](https://newq.net/publications/more/modifying-compressed-voxels) publication contains for as:
- Read-memory: The data-pool (`pool`) & the page-table (`lut`).
- Allocation-memory: Lengths of each bucket & the page-table.

When one would talk about write-memory in this context it would refer to allocation-memory.

#### Virtual pointers

Virtual pointers are constructed from a base-pointer & an offset. The base-pointer is the product from a level & a (part of the) hash of the node (or the bucket). The offset is defined by previous inserts (due to hash-collisions) & allocation behavior. It may or may not be deterministic, but it generally is a pain in the ass (as you can see in [this section](#lookup-optimization)).

So to a certain extent virtual pointers:
- Tell you where something is.
- Tell you what something is (*because hash*).

### Lookup optimization

One can assume each element of the node is known on lookup and thus the hash is too. Being able to compute the base-pointer, the challenge is figuring out the offset. In this section I'll discuss multiple solutions which help optimize lookups.

#### When do you *not* optimize

One does not need it when your configuration is such that you guarantee very little hash-collisions already. One can realize this by:
1. Configuring 64-bit addressable space as 32-bit addressable space means only 32-bit hashing, which does not allow for a lot of nodes to be hashed (if you want few collisions). Thus your MVM needs to be large too as to allow for a lot of buckets a level, increasing hash utilization. Else it does not matter how big your hashes are.
2. Configuring real memory to be comparatively tiny as to decrease the amount of nodes to hash, decreasing chances of collisions furthermore.

It also means that:
1. The space allocated will be twice as big as that of 32-bit aligned nodes as nodes are essentially a header with a bag of pointers.
2. Pages will probably need to be configured to be smaller to compensate for a lot of waste which will have a negative performance impact.

#### Tier 1 caching

This one is simple. You just memoize the virtual pointers of nodes you force to be present at all times & which should be accessed a lot. The perfect example of this are full nodes. The DAG is not properly initialized without them and the cache has hard-coded constraints knowing this.

#### Tier 2 caching

Something like a LUT with a limited amount of entries... I have not worked this out and maybe never will.

#### General optimization: bins

Constraints & requirements:
- Read-memory should be left (relatively) untouched.
- Lookup-memory can increase, but at most by as much as there is read-memory.
- Lookups need to be faster of course.

As discussed above, not all bits of a hash can be used to generate a base-pointer, but it may be possible to leverage more on the procedural side (not to waste CPU-cycles). To do so some or all left-over bits can be used to index into a bin which yields you either a valid or invalid address. You know if no match is found when the address is invalid. Assuming hash-collisions are still possible, if the address is valid, it'd mean you have a *candidate* which should be matched. If the match is false you look for the next item in the bin and you do this until either a match is found or you are returned an invalid address.

Some things of note:
- Nodes are not of fixed length, but are sized possibly 9 words long.
- The amount of collisions in a bin can be guessed, but is not known.
- The amount of possible bins in a bucket is the same as the amount of permutations possible with the hash-bits reserved to identifying a bin.

Knowing what's been mentioned one may realize bins can not be stored as arrays. It'd have it's advantages, but to realize this at least half the MVM needs to be allocated (which translates to real memory, due to memory access patterns). Why? Assuming a perfect hash-function along with a minimum node-size of 64-bit and support for buckets containing only 64-bit nodes, one requires a bin for each node with the bin being half the size of a node. I.e. the total amount of bins equals that of half the total amount of nodes. I.e. you need half of MVM.

Having explored dynamic allocation before (as preparation for supporting free-operations) the solution seemed obvious: singly linked lists. One can -- for each bin-list -- allocate a head-pointer which may point to a node. This node may in turn point to another which in turn may point to another and so on. Voilla, linked list. The actual implementation involves creating a dense array with for each bin a head-pointer. The next-pointers can be stored right after each node in the DP. Eventually this means your DP most likely increases by ~20% (depending on the data). Cache-efficiency is great too as reading the list means you automatically cached the node to match.

This particular solution has two things of note:
1. Supporting free-ing on the HashDAG requires that you unlink nodes from the bin-list too. This means that you need both the previous and next nodes whilst only the next-pointer is saved, thus it requires walking the list. This sounds awful, but it is no big deal if all lists are generally tiny. To achieve this the bin-count needs to be increased. It is thus recommended to try that first before resorting to doubly-linked lists.
2. This solution increases read-memory by ~20% in most cases. This is not such a big deal, but it could be improved upon at the cost of total memory & cache-efficiency. One can create a pointer-heap exactly half the size of the DP (allocating at least one pointer a node). It is then possible to map each pointer procedurally to the DP. ([Something similar is used for implementing free.](#specifics-encoding--layouts)) As a trade-off the footprint has shifted from ~120% to ~150% in total memory & any cache-miss during lookup can happen twice now, but read-memory is only 100%. I personally haven't implemented both methods and compared them, but it turns out that changing the minimum node-size has a big impact on the complexity of a [featureful dynamic allocator](#featureful-dynamic-allocator).

### Staging

Without much research it seems there are 3 staging methods:
1. Checksumming buffer-chunks to determine which chunk has changed and proceeding from there.
2. Mapping a dense bit-array with each bit indicating a chunk which has changed.
3. Using ranges indicating which regions have changed.

Now as for their properties:
1. It is the best solution if one is just handed a buffer with no way of getting notified on any change.
2. It is a great solution if resolution doesn't matter much. Just flipping a bit is probably cheapest too.
3. Great if you want to know exactly what changed or if you expect a lot of sequential writes. It can be as complex as you want it to be.

I personally tried using both the second and third method, but considering the workload it may come as no surprise bit-arrays turned out to be on top. As for how slow ranges proved to be: I have mentions in my notes saying it was "exactly twice as slow". I remember testing it on both heavy and light workloads. A code-review shows I merged regions just-in-time and not eagerly. Considering this, staging can probably be improved upon at the cost of HashDAG write-speed. Speaking of writes: I do not remember either method having a real impact on writes. Certainly the bit-array has no measurable impact.

### Purging

Two ways of purging unused nodes have been identified:
1. Garbage-collection: Create a second HashDAG and re-apply all useful nodes on a fixed or variable tick after which the old is discarded.
2. Featureful dynamic allocator: A dynamic allocator which isn't constrained to COW-semantics and allows for free-ing resources.

#### Garbage collection

This is the same concept mentioned in the paper about modifiable SVDAGs. In essence one creates a second HashDAG and imports all DAGs which are still in use. After the import the old is discarded.

During GC writes are completely halted which makes this a poor choice when edits need to be seamless at all times. It is also quite demanding & requires staging the whole HashDAG to finish the job which makes it even less appealing for very interactive applications such as games.

#### Featureful dynamic allocator

In order to enable free-ing, the system performing this work needs to know first what is allocated. This is the job of the free-store. Without this it is not possible to start tackling this problem. This free-store should know both what nodes & pages are stored so that both nodes & pages can be free-ed again.

##### The page-table

Starting with the simplest part, the PT: The design is quite simple and lazy really. The page-table acts out its allocations in exactly the same way as it normally does by just maintaining a cursor, but it contains a list too which captures all deletions. To prevent this from becoming a real problem, every time an allocation happens the list is tried first. The only way the list can grow to a large capacity is if the amount of page-deletions is out of proportion.

Keeping track of nodes however is not as trivial...

##### Basics of: allocating nodes

A segregated doubly-linked list for each bucket is maintained containing all free blocks of the DP. This list is segregated over the block-sizes. These can be classified under two types:
- **Sized blocks:** These are blocks which can contain nodes of all flavours and sizes nodes (interior ([1-8] children) & exterior). Each has its dedicated free-list.
- **Large blocks:** These are blocks which are larger but no bigger than a page. It is segregated over block-sizes which are odd and even.

This list itself is encoded in free nodes & bounds are encoded in a heap mapped to the DP. On top of this a head is required for each list, but no tails. Finally there is a need to keep track of what nodes are referenced. One can not implement free without this as you need to know what nodes are in use. These RCs are best encoded with the nodes (not as attributes as it does not depend on parent nodes). Because of this -- and deletion & addition being mutually exclusive -- the same heap for storing bounds can be used to encode them, which saves space.

Ignoring encoding for now, here is a high level of what is the allocation-workflow of any system supporting free:
1. When allocating a page: Lazily initialize it as a *large block* and register it with the free-list.
2. When a node is allocated large blocks are tried first, then sized blocks. Whatever is available first is popped & split when possible. The overextended part is registered to the free-list again.
3. Finally, RCs are initialized to 1.

**A remark on RCs:** RCs are *only* incremented on node-reference. This means both that:
- Individual pointer-references do not affect the RC. E.g. a HashDAG containing only full-nodes has for each downstream node RC=1 and not RC=8. This is because these nodes are referred to by only one node (or once by each *unique* pointer per node).
- External references do not affect the RC. It'd otherwise mean one has to take one more variable into account during design. Neither is it very practical to propagate an external reference through the tree.

##### Basics of: free-ing nodes

With free-ing, the RC is more involved. Free-ing is essentially decrementing the RCs down the DAG and for each RC=1 the owner is deleted. **This does not mean the DAG is deleted.** Deleting all nodes downstream would be OK for trees but due to the nature of a DAG it is not very predictable if any nodes downstream are shared (and having to visit to delete prevents loop-unrolling).

*For any t(h)inkers (problem without solution?):* Predicting RC=1 requires indirect communication between a given node and a set with at least all future and current upstream nodes. An idea: find a common pattern amongst this set (such as levels -> MVM-range) on which to broadcast hints for this set. (It's practicality should depend on the value and size of these hints and how targeted the broadcasts can get.)

As for deletion: When a node is ready to be deleted a scan happens to the left & right of the node *in the page*. Any free blocks are merged with the now deleted node. During this merge both the list and the new block need to be updated. Finally, if this block happens to be of the same size as the page you now the page is free which means that the page can be free-ed.

##### Specifics: Padding

There are a couple of free-ing implementations possible, but almost none of them can avoid padding. The two I personally found (interesting):
- Allocating nodes to blocks which are a multiple of the minimum node-size (assuming the page is a multiple too). It can be a reasonable implementation if the minimum node-size is 2 as it is quite trivial and fast with the trade-off being memory-efficiency.
- Allocating using best-fit algorithms. Here padding is quite valuable when the best-fit block has an excess smaller than the minimum node-size. Most likely one can afford ignoring it as a fit altogether however, but padding still proves valuable.

Padding is something which *does* need attention as it needs to be recognized as such both during reads and deletion. If padding is ignored it could yield corrupted reads or memory leaks sprinkled with fragmentation. Thus in practice nodes are padded in front. This way the allocator only needs to offset the pointer and the node is read the same as without padding. Aside from this it is best practice to give padding a globally dedicated code to prevent any (future) UB (e.g. `!0 - 1`). It does mean that addressing needs to be adjusted not to overlap.

##### Specifics: Encoding & layouts

Throughout I will assume the minimum node-size is 2. Anything larger complicates matters:

The heap containing both (32-bit) RCs & bounds is procedurally mapped to the DP as any compression is thought to prove of little value. Thus each node contains has a word mapped in the least (but not always). This means that the heap does not need to be as big as the DP. It can be half the size as the minimum node-size is 2. A quirk this brings however is aliasing. Because of aliasing it is important to be consistent in how node-values are mapped to the heap. If this is not done with care one could map a single value to two nodes.

During scanning (used in merging or sequential lookups) it is required to get a mapping to the allocation-data to figure out if it is free and what the bounds are. This scanning process can happen both from the left & the right (so just reading the child-mask is not enough). Enabling this mapping can be done in a variety of ways, but the simplest one is to just initialize allocation-data on both the start & end index of the block. This may have two mappings to the same value for small nodes, but it's something which can be ignored (by either the implementor or the algorithm).
To tell:
- What the bounds are: the bounds are written for both sized & large blocks.
- If the block is free: the MSB is dedicated to an allocation-flag. [This reduces the bits that can be used for RCs](#appendix-rcs).

**Final remark:** As with mapping, strict rules have to be employed to use the RC as there are possibly 2 for a given node due to aliasing. This can be avoided by mapping differently, but this is simpler & cheaper, but it does leaf more room for mistakes.

##### Specifics: Statistics

I did not yet measure any implementation to the extent that I can report on it. Currently only a best-fit allocator has been implemented as PoC, but neither is it truly finished and neither have I implemented the "faster" allocator which -- for all I know -- might not be much faster after all.

So far I can report that one can expect allocations to take less than twice as long with the most feature-rich allocator discussed.

##### Appendix: RCs

Something of note is that RCs are not able to overflow *if it is used strictly for counting node-references*. Here's why:
1. Deriving from the worst case scenario being: a level with its own 32-bit address-space filled up to the brink with interior-nodes of one and the same child.
2. In this case actual node-addressing space is only 31 bit as `addressing space / minimum size of node = 2^31 nodes`.
3. Which is exactly the same as for how often this single child is referenced. (As mentioned before, RCs count node-references, not pointer-references.)

**However** one may want to *leak* nodes by adding a non-existent reference. As the RC will never reach 0 it can never be deleted. This is great if you want something like [tier 1 cache](#tier-1-caching). In this case it would be possible in such a scenario, yet unlikely. To be certain one can either restrict address space or keep track of leaks, not to increment more than necessary. Either way it may be interesting to keep track of leaks in case they don't need to *actually* leak and only need to live for a given span of time.

Finally, as for external references it has been mentioned that RCs should not be used for this (as it is not accounted for and expensive). Instead it is advised to provide references or smart-pointers which are not tied in with the "kernel".

### Concurrency

- Sharding of graph & lock-shards mapped to 'x' buckets.
- Page allocations are sequential (using one mutex).

## Missing documentation

As mentioned above, this is mainly for myself and my future-self so this document is certainly not complete and it will probably never discuss more than what is implemented / has been tried as I have more "notes" detailing other concepts.
