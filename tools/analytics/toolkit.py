from analytics import utils
from analytics.constants import CLASS, CONFIG, HASH_DAG
import analytics.BasicHashDAG as BHD
import analytics.HashDAG as HD


'''Could probably be automated, but I opt to add manually (= flexible).'''
toolkit = {
    'HashDAG': {
        HD.general_stats.__name__: HD.general_stats,
        HD.print_config.__name__: HD.print_config,
    },
    'BasicHashDAG': {
        BHD.bucket_occupancy_bar.__name__: BHD.bucket_occupancy_bar,
        BHD.allocated_pages_stack.__name__: BHD.allocated_pages_stack,
    }
}

'''Don't like the shorthands or there are collisions? Specify an alias here.'''
alias_overrides = {
    # 'alias': 'full_tool_name',
}


def to_alias(tool_name):
    '''Gets all aliases for a tool name.'''
    for k, v in aliases().items():
        if v == tool_name:
            yield k


def to_shorthand(tool_name):
    '''First letter of each word. (E.g. pizza_box -> pb)'''
    return ''.join(
        word[0] if 1 <= len(word) else ''
        for word in tool_name.split('_')
    )


def aliases():
    '''Generated shorthands + overrides.'''
    aliases = {}
    for tools in toolkit.values():
        aliases.update(dict(
            (to_shorthand(tool), tool) for tool in tools.keys()
        ))
    aliases.update(alias_overrides)
    return aliases


def function_arguments(tool):
    '''Ignores arguments deemed to be inner-state and stops on `_`. (Tiny function.)'''
    ignored = [CONFIG, HASH_DAG, CLASS]
    for arg in tool.__code__.co_varnames:
        if arg in ignored:
            continue
        if arg == '_':
            break
        if arg.startswith('_'):
            continue
        yield arg


def run(tool_name, state, default):
    '''Runs the given tool (if in class) from given state. Can supply a default if not found.'''
    # `HashDAG` as fallback:
    tool = toolkit.get(state[CLASS], lambda _: toolkit['HashDAG'])
    tool.update(toolkit['HashDAG'])  # inherit `HashDAG`
    tool_name = aliases().get(tool_name, tool_name)
    return tool.get(tool_name, default)(**state)
