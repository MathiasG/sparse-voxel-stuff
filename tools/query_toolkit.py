#!/usr/bin/python3

from analytics.toolkit import toolkit, function_arguments, to_alias

'''
Displays available tools, their aliases & arguments. Handy for CLI tools.
For specific queries use existing tools like less.
'''


def main():
    for dag_class in toolkit:
        print(f'{dag_class}:')
        for tool_name in toolkit[dag_class]:
            tool = toolkit[dag_class][tool_name]
            print(f' * {tool_name}: {tool.__doc__}')
            arguments = [arg for arg in function_arguments(tool)]
            if len(arguments) != 0:
                print(f'   Argument(s): {", ".join(arguments)}')
            aliases = [alias for alias in to_alias(tool_name)]
            if len(aliases) != 0:
                print(f'   Alias(es): {", ".join(aliases)}')
        print()


if __name__ == '__main__':
    main()
