use crate::UserPreferences;
use ::{
    nalgebra::{Isometry3, Translation, UnitQuaternion, Vector3},
    std::{mem::size_of, slice::from_raw_parts},
};

pub fn new_child_lut() -> [u32; 2048] {
    let mut child_lut = [0; 2048];
    for directions in 0..8 {
        for intersections in 0..256 {
            for i in 0..8 {
                let child = i ^ directions;
                if intersections & (1 << child) != 0 {
                    child_lut[(directions * 256 + intersections) as usize] = child;
                    break;
                }
            }
        }
    }
    child_lut
}

#[inline]
pub fn new_model_xform(levels: u32) -> Isometry3<f32> {
    Isometry3::from_parts(
        Translation::from(Vector3::from([-(1 << (levels - 1)) as _; 3])),
        UnitQuaternion::identity(),
    )
}

#[inline]
pub fn new_eye_xform(pref: &UserPreferences) -> Isometry3<f32> {
    // I perturbed the view angle a bit not to get whole numbers.
    Isometry3::new(pref.start_pos_eye.into(), [1e-4; 3].into())
}

#[inline]
/// # Safety
///
/// This just reads the pointer using from_raw_parts for size_of::<T>() long, byte-aligned.
/// Safety-wise, everything which applies to from_raw_parts applies here too.
/// Seemingly, the most important part of which is that the data shouldn't _just_ be contiguous in memory.
/// It also needs to be a single allocated object.
pub unsafe fn any_as_u8_slice<T: Sized>(p: &T) -> &[u8] {
    from_raw_parts((p as *const T) as *const u8, size_of::<T>())
}
