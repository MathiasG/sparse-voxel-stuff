use super::{
    hash_dag::{
        constants::{LEAF_LEVEL, SUPPORTED_LEVELS},
        utils::{bottom_child_mask, descend, upper_child_mask},
        HashDAG, Result,
    },
    shared_xform::SharedTransform,
    utils::new_child_lut,
    Model,
};
use ::{
    nalgebra::{Matrix3, Vector3},
    num_traits::identities::Zero,
    std::ops::{Deref, DerefMut},
};

const CHILD_MASK: u32 = 0xff;
const INTERSECT_MASK: u32 = 0xff00;

pub struct RTCache {
    pub vptr: u32,
    pub level: u32,
    pub path: Vector3<u32>,
    pub invalidated: bool,

    rot: Matrix3<f32>,
    pl_dir_rcp: Vector3<f32>,
    ray_dir: Vector3<f32>,
    directions: u32,
    stack: [(u32, u32); LEAF_LEVEL as _],
    child_lut: [u32; 2048],
}

impl RTCache {
    pub fn new(model: &Model, ray_dir: Vector3<f32>) -> Self {
        let rot = Self::model_rot(model);
        let (pl_dir_rcp, directions) = Self::ray_state(model, ray_dir);
        Self {
            vptr: model.vptr,
            level: model.level(),
            path: Vector3::zero(),
            pl_dir_rcp,
            rot,
            ray_dir,
            directions,
            stack: [(0, 0); LEAF_LEVEL as _],
            child_lut: new_child_lut(),
            invalidated: false,
        }
    }
    #[inline]
    pub fn refresh(&mut self, model: &Model) {
        self.vptr = model.vptr;
        self.level = model.level();
        self.path = Vector3::zero();
        self.invalidated = false;
    }
    #[inline]
    fn model_rot(model: &Model) -> Matrix3<f32> {
        model.rot().to_rotation_matrix().into()
    }
    #[inline]
    fn ray_state(model: &Model, ray_dir: Vector3<f32>) -> (Vector3<f32>, u32) {
        let pl_dir = model.rot().inverse() * ray_dir;
        (
            pl_dir.map(|v| 1.0 / v),
            pl_dir.map(|c| (c < 0.0) as u32).dot(&Vector3::new(4, 2, 1)),
        )
    }
    pub fn renew(&mut self, model: &Model, ray_dir: Vector3<f32>) {
        self.ray_dir = ray_dir;
        self.rot = Self::model_rot(model);
        let (pl_dir_rcp, directions) = Self::ray_state(model, ray_dir);
        self.pl_dir_rcp = pl_dir_rcp;
        self.directions = directions;
        self.refresh(model);
    }
}

pub struct RayTracer<'a> {
    pub ray: &'a SharedTransform,
    pub model: &'a Model,
    pub cache: &'a mut RTCache,
}

impl Deref for RayTracer<'_> {
    type Target = RTCache;

    #[inline]
    fn deref(&self) -> &Self::Target {
        self.cache
    }
}

impl DerefMut for RayTracer<'_> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.cache
    }
}

impl<'a> RayTracer<'a> {
    #[inline]
    pub fn new(ray: &'a SharedTransform, model: &'a Model, cache: &'a mut RTCache) -> Self {
        Self { ray, model, cache }
    }
    #[inline]
    fn next_child(&self, intersected: u32) -> u32 {
        self.child_lut[(self.directions * 256 + intersected) as usize]
    }
    #[inline]
    fn x_split(&self, hit_off: &Vector3<f32>) -> u32 {
        let s = (0.0 <= hit_off.dot(&self.rot.column(0))) as u32;
        s * 0b11110000 + (1 - s) * 0b00001111
    }
    #[inline]
    fn y_split(&self, hit_off: &Vector3<f32>) -> u32 {
        let s = (0.0 <= hit_off.dot(&self.rot.column(1))) as u32;
        s * 0b11001100 + (1 - s) * 0b00110011
    }
    #[inline]
    fn z_split(&self, hit_off: &Vector3<f32>) -> u32 {
        let s = (0.0 <= hit_off.dot(&self.rot.column(2))) as u32;
        s * 0b10101010 + (1 - s) * 0b01010101
    }

    pub fn trace<DAG: HashDAG>(&mut self, dag: &DAG, max_steps: u32) -> Result<&Self> {
        if self.invalidated {
            return Err(String::from(
                "The RT cache has been invalidated. Update it before tracing the ray.",
            ));
        }
        let mut masks = dag.get(self.vptr)? & CHILD_MASK;
        masks |= (masks & self.comp_intersect_mask()) << 8;
        for _ in 0..max_steps {
            {
                let mut new_level = self.level;
                while self.model.level() < new_level && 0 == (masks & INTERSECT_MASK) {
                    new_level -= 1;
                    let (v, m) = self.stack[new_level as usize];
                    self.vptr = v;
                    masks = m;
                }
                if new_level == self.model.level() && 0 == (masks & INTERSECT_MASK) {
                    self.path = [!0; 3].into();
                    break;
                }
                self.path = self.path.map(|v| v >> (self.level - new_level));
                self.level = new_level;
            }

            let intersected = (masks & INTERSECT_MASK) >> 8;
            let child = self.next_child(intersected);
            let child_bit = 1 << child;
            masks &= !(child_bit << 8);
            let entry = self.level as usize;
            self.stack[entry] = (self.vptr, (child << 16) | masks & 0xffff);
            self.level += 1;
            self.path = descend(&self.path, child);
            self.vptr = {
                let child_offset = (masks & (child_bit - 1)).count_ones() + 1;
                dag.get(self.vptr + child_offset)?
            };

            if self.level < LEAF_LEVEL {
                masks = dag.get(self.vptr)? & CHILD_MASK;
                masks |= (masks & self.comp_intersect_mask()) << 8;
            } else {
                let leaf = dag.leaf(self.vptr)?;
                let child_mask = upper_child_mask(leaf) as u32;
                let mut intersected = child_mask & self.comp_intersect_mask();
                for _ in 0..8 {
                    let child = self.next_child(intersected);
                    intersected &= !(1 << child);
                    let child_mask = bottom_child_mask(leaf, child) as u32;
                    self.path = descend(&self.path, child);
                    self.level += 1;
                    {
                        let intersected = child_mask & self.comp_intersect_mask();
                        if intersected != 0 {
                            let child = self.next_child(intersected);
                            self.path = descend(&self.path, child);
                            self.level += 1;
                            debug_assert_eq!(self.level, SUPPORTED_LEVELS);
                            self.invalidated = true;
                            return Ok(self);
                        }
                        self.path = self.path.map(|v| v >> 1);
                        self.level -= 1;
                    }
                    if intersected == 0 {
                        break;
                    }
                }
                masks = 0;
            }
        }
        self.invalidated = true;
        Ok(self)
    }

    fn comp_intersect_mask(&self) -> u32 {
        let scale = SUPPORTED_LEVELS - self.level;
        let extent = (1 << (scale - 1)) as f32;
        let pos = self.path.map(|v| extent + (v << scale) as f32);
        let obb_mid = self.model.rot() * pos + self.model.pos();
        let ray_to_mid = obb_mid - self.ray.pos();
        let t_mid = self.rot.tr_mul(&ray_to_mid).component_mul(&self.pl_dir_rcp);
        let t_extents = extent * self.pl_dir_rcp.abs();
        let t_min = (t_mid - t_extents).max().max(0.0);
        let t_max = (t_mid + t_extents).min();
        if t_max <= t_min {
            0
        } else {
            let hit_off = ((t_min + t_max) * 0.5 * self.ray_dir) - ray_to_mid;
            let pl_hit_off = self.model.rot().inverse() * hit_off;
            let hit_idx = pl_hit_off
                .map(|c| (0.0 <= c) as u32)
                .dot(&Vector3::new(4, 2, 1));
            let mut intersections = 1 << hit_idx;
            if t_min <= t_mid.x && t_mid.x <= t_max {
                let hit_off = t_mid.x * self.ray_dir - ray_to_mid;
                intersections |= self.y_split(&hit_off) & self.z_split(&hit_off);
            }
            if t_min <= t_mid.y && t_mid.y <= t_max {
                let hit_off = t_mid.y * self.ray_dir - ray_to_mid;
                intersections |= self.x_split(&hit_off) & self.z_split(&hit_off);
            }
            if t_min <= t_mid.z && t_mid.z <= t_max {
                let hit_off = t_mid.z * self.ray_dir - ray_to_mid;
                intersections |= self.x_split(&hit_off) & self.y_split(&hit_off);
            }
            intersections
        }
    }
}
