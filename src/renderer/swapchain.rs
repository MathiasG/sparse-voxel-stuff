use ash::{
    extensions::khr::Surface,
    vk::{
        ColorSpaceKHR, Extent2D, Format, PhysicalDevice, PresentModeKHR, SurfaceCapabilitiesKHR,
        SurfaceFormatKHR, SurfaceKHR,
    },
};

pub struct SwapchainSupportDetails {
    pub capabilities: SurfaceCapabilitiesKHR,
    pub formats: Vec<SurfaceFormatKHR>,
    pub present_modes: Vec<PresentModeKHR>,
}

impl SwapchainSupportDetails {
    pub unsafe fn new(device: PhysicalDevice, surface: &Surface, surface_khr: SurfaceKHR) -> Self {
        let capabilities = surface.get_physical_device_surface_capabilities(device, surface_khr);
        let formats = surface.get_physical_device_surface_formats(device, surface_khr);
        let present_modes = surface.get_physical_device_surface_present_modes(device, surface_khr);
        Self {
            capabilities: capabilities.unwrap(),
            formats: formats.unwrap(),
            present_modes: present_modes.unwrap(),
        }
    }

    pub fn choose_swapchain_surface_format(
        available_formats: &[SurfaceFormatKHR],
    ) -> SurfaceFormatKHR {
        let desired_format = SurfaceFormatKHR {
            format: Format::B8G8R8A8_UNORM,
            color_space: ColorSpaceKHR::SRGB_NONLINEAR,
        };
        if available_formats.len() == 1 && available_formats[0].format == Format::UNDEFINED {
            desired_format
        } else {
            *available_formats
                .iter()
                .find(|&&format| format == desired_format)
                .unwrap_or(&available_formats[0])
        }
    }

    fn choose_swapchain_surface_present_mode(
        available_present_modes: &[PresentModeKHR],
    ) -> PresentModeKHR {
        if available_present_modes.contains(&PresentModeKHR::MAILBOX) {
            PresentModeKHR::MAILBOX
        } else if available_present_modes.contains(&PresentModeKHR::FIFO) {
            PresentModeKHR::FIFO
        } else {
            PresentModeKHR::IMMEDIATE
        }
    }

    fn choose_swapchain_extent(
        capabilities: SurfaceCapabilitiesKHR,
        preferred_extent: Extent2D,
    ) -> Extent2D {
        if capabilities.current_extent.width != std::u32::MAX {
            capabilities.current_extent
        } else {
            let min = capabilities.min_image_extent;
            let max = capabilities.max_image_extent;
            let width = preferred_extent.width.min(max.width).max(min.width);
            let height = preferred_extent.height.min(max.height).max(min.height);
            Extent2D { width, height }
        }
    }

    pub fn get_ideal_swapchain_properties(
        &self,
        preferred_extent: Extent2D,
    ) -> SwapchainProperties {
        let format = Self::choose_swapchain_surface_format(&self.formats);
        let present_mode = Self::choose_swapchain_surface_present_mode(&self.present_modes);
        let extent = Self::choose_swapchain_extent(self.capabilities, preferred_extent);
        SwapchainProperties {
            format,
            present_mode,
            extent,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SwapchainProperties {
    pub format: SurfaceFormatKHR,
    pub present_mode: PresentModeKHR,
    pub extent: Extent2D,
}
