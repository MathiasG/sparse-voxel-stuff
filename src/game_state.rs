use super::{
    app_info::*,
    hash_dag::{
        constants::{LEAF_LEVEL, SUPPORTED_LEVELS},
        conversion::Converter,
        editing::{
            shapes::{Sphere, AABB},
            Editor,
            Operation::{self, *},
            Shape,
        },
        hash_table::basic::HashTable,
        prelude::*,
        reporting::Reporter,
        shared_hash_dag::SharedHashDAG,
        shmem_config::*,
        tracking::{basic::*, Tracker},
        utils::serialization::{read_exact_slice, read_word},
        HashDAG, HashDAGMut,
    },
    model::Model,
    renderer::debug::ENABLE_VALIDATION_LAYERS,
    shared_xform::SharedTransform,
    utils::{new_eye_xform, new_model_xform},
    UserPreferences,
};
use ::{
    nalgebra::Vector3,
    renderdoc::{CaptureOption, RenderDoc, V141},
    std::{
        cmp::Ordering,
        fs::{create_dir_all, File},
        io::Read,
        mem::size_of,
        path::Path,
        time::SystemTime,
    },
};

pub type State<'shmem> = GameState<HashTable<'shmem>, BasicTracker>;
pub struct GameState<DAG: HashDAG, T: Tracker> {
    pub dag: SharedHashDAG<DAG, T>,
    pub eye: SharedTransform,
    pub models: Vec<Model>,
    pub target: Vector3<u32>,
    pub edit: Option<(Operation, Vector3<u32>)>,
    pub rd: Option<RenderDoc<V141>>,
    pub ts: SystemTime,
    _config: ShmemConfig,
}

impl State<'_> {
    pub fn new(pref: &UserPreferences) -> Self {
        log::info!("Initializing game state...");
        let (root, config) = {
            create_dir_all(FLINK_STORE).expect("Could not create directory.");
            let config = ShmemConfig {
                path: format!("{}main-config.json", FLINK_STORE),
                class: stringify!(BasicHashDAG).into(),
            };
            (
                Some(format!("{}main-", FLINK_STORE)),
                config.write().expect("Failed to write the config file."),
            )
        };
        let mut dag = match BasicHashDAG::with_capacity(root.as_ref(), pref.pool_mem()) {
            Ok(dag) => dag,
            Err(e) => {
                if e.cmp(&"A HashDAG with the same file link already exists.".into())
                    == Ordering::Equal
                {
                    panic!(format!("The application did not exit properly last time. Purge the folder {} and try again.", FLINK_STORE));
                } else {
                    panic!(e);
                }
            }
        };
        let mut load_svdag = |file, stop| {
            let mut model = {
                let bd = BasicDAG::from_file(Path::new(SVDAG_STORE).join(file))
                    .expect("Can't open file.");
                Model::from_vptr(dag.import_strict(&bd, stop).unwrap())
            };
            model.xform = new_model_xform(model.levels()).into();
            model
        };
        let model = match pref.demo {
            0 => {
                let mut file = File::open("assets/lantern.comp.bin").unwrap();
                file.read_exact(&mut [0; size_of::<u64>() * 6]).unwrap();
                let levels = read_word(&mut file).unwrap();
                let pool_len = read_word(&mut file).unwrap() as usize;
                file.read_exact(&mut [0; size_of::<u32>() * 6]).unwrap();
                let mut model = {
                    let pool = read_exact_slice(&mut file, pool_len).unwrap();
                    let basic = BasicDAG::new(levels, pool);
                    Model::from_vptr(dag.import_strict(&basic, None).unwrap())
                };
                model.xform = new_model_xform(model.levels()).into();
                model
            }
            1 => {
                const LEVELS: u32 = 12;
                const LEVEL: u32 = SUPPORTED_LEVELS - LEVELS;
                let mut vptr = dag.full_node_ptr(LEAF_LEVEL).unwrap();
                let mut interior = [1, vptr];
                let mut n = Pass(&interior);
                for level in (LEVEL..LEAF_LEVEL).rev() {
                    vptr = dag.add_interior(level, n, n.hash_as_interior()).unwrap();
                    interior = [1, vptr];
                    n = Pass(&interior);
                }
                const EXTENT: u32 = 1 << (LEVELS - 1);
                let sphere = Sphere::new(&[EXTENT; 3].into(), EXTENT as _);
                vptr = dag.edit(vptr, Link, &sphere).unwrap();
                Model::from_vptr(vptr)
            }
            2 => load_svdag(format!("epiccitadel128k{}", DAG_SUFFIX), Some(14)),
            3 => load_svdag(format!("sponza16k{}", DAG_SUFFIX), None),
            4 => load_svdag(format!("sanmiguel16k{}", DAG_SUFFIX), None),
            5 => {
                const LEVELS: u32 = SUPPORTED_LEVELS;
                const EXTENT: u32 = 1 << (LEVELS - 1);
                const BOX_EXTENT: u32 = 1 << 4; // if too small you can notice the view matrix error
                let mut vptr = dag.full_node_ptr(SUPPORTED_LEVELS - LEVELS).unwrap();
                let mut pos = Vector3::new(EXTENT, EXTENT, 0);
                for _ in 0..=((EXTENT << 1) / (BOX_EXTENT << 1)) {
                    let aabb = AABB::new(&pos, BOX_EXTENT);
                    vptr = dag.edit(vptr, Unlink, &aabb).unwrap();
                    pos.z += BOX_EXTENT << 1;
                }
                let mut xform = new_model_xform(LEVELS);
                xform.translation.vector.z += EXTENT as f32;
                Model::new(vptr, xform)
            }
            _ => {
                let level = pref.demo as u32 % 10;
                Model::new(
                    dag.full_node_ptr(level).unwrap(),
                    new_model_xform(SUPPORTED_LEVELS - level),
                )
            }
        };
        log::info!("DAG report: {:?}", dag.report());
        let mut rd: Option<RenderDoc<V141>> = if pref.load_renderdoc {
            RenderDoc::new().ok()
        } else {
            None
        };
        if let Some(ref mut rd) = rd {
            rd.set_capture_option_u32(
                CaptureOption::DebugOutputMute,
                (!ENABLE_VALIDATION_LAYERS) as u32,
            );
            rd.set_capture_file_path_template(CAPTURES_PATH);
        }
        log::debug!("Done initializing game state.");
        Self {
            dag,
            models: vec![model],
            eye: SharedTransform::from(new_eye_xform(pref)),
            target: [!0; 3].into(),
            edit: None,
            ts: SystemTime::now(),
            rd,
            _config: config,
        }
    }
}
