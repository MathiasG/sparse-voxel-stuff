use ash::{version::DeviceV1_0, vk, Device};

pub struct Buffer {
    pub buffer: vk::Buffer,
    pub memory: vk::DeviceMemory,
    pub size: vk::DeviceSize,
}

impl Buffer {
    pub unsafe fn new(
        vk_context: &super::VkContext,
        size: vk::DeviceSize,
        usage: vk::BufferUsageFlags,
        mem_properties: vk::MemoryPropertyFlags,
    ) -> Self {
        let device = vk_context.device();
        let buffer = {
            let buffer_info = vk::BufferCreateInfo::builder()
                .size(size)
                .usage(usage)
                .sharing_mode(vk::SharingMode::EXCLUSIVE)
                .build();
            device.create_buffer(&buffer_info, None).unwrap()
        };
        let mem_requirements = device.get_buffer_memory_requirements(buffer);
        let memory = {
            let mem_type = Self::find_memory_type(
                mem_requirements,
                vk_context.get_mem_properties(),
                mem_properties,
            );
            let alloc_info = vk::MemoryAllocateInfo::builder()
                .allocation_size(mem_requirements.size)
                .memory_type_index(mem_type)
                .build();
            device.allocate_memory(&alloc_info, None).unwrap()
        };
        device.bind_buffer_memory(buffer, memory, 0).unwrap();
        let size = mem_requirements.size;
        Self {
            buffer,
            memory,
            size,
        }
    }

    fn find_memory_type(
        requirements: vk::MemoryRequirements,
        mem_properties: vk::PhysicalDeviceMemoryProperties,
        required_properties: vk::MemoryPropertyFlags,
    ) -> u32 {
        for i in 0..mem_properties.memory_type_count {
            if requirements.memory_type_bits & (1 << i) != 0
                && mem_properties.memory_types[i as usize]
                    .property_flags
                    .contains(required_properties)
            {
                return i;
            }
        }
        panic!("Failed to find suitable memory type.")
    }

    pub unsafe fn drop(&mut self, device: &Device) {
        device.free_memory(self.memory, None);
        device.destroy_buffer(self.buffer, None);
    }
}

impl From<(vk::Buffer, vk::DeviceMemory, vk::DeviceSize)> for Buffer {
    #[inline]
    fn from((buffer, memory, size): (vk::Buffer, vk::DeviceMemory, vk::DeviceSize)) -> Self {
        Buffer {
            buffer,
            memory,
            size,
        }
    }
}
