use self::{
    app_info::*,
    hash_dag::{
        constants::{COLOR_TREE_LEVELS, LEAF_LEVEL, SUPPORTED_LEVELS, TOTAL_PAGES},
        conversion::Converter,
        hash_table::basic::HashTable,
        prelude::{BasicDAG, Node, Pass, Strict},
        shared_hash_dag::SharedHashDAG,
        shmem_config::ShmemConfig,
        staging::Staging,
        tracking::{basic::*, Tracker},
        utils::{
            count_leaves, serialization::load_ron, serialization::read_exact_slice,
            serialization::read_word,
        },
        HashDAG, HashDAGMut, Result,
    },
};
use ::std::{cmp::Ordering, fs::File, io::Read, path::Path, time::SystemTime};

mod app_info;
mod hash_dag;

#[allow(dead_code)]
impl BasicHashDAG<'_> {
    fn put_no_seek(
        &mut self,
        pool: Node,
        level: u32,
        ptr: usize,
        visits: &mut [u32],
    ) -> Result<(u32, Option<u32>)> {
        let is_visited = *visits.get(ptr).ok_or(format!(
            "Node index out of bounds! Level: {}, Pointer: {}",
            level, ptr
        ))? != !0;
        if is_visited {
            if level == LEAF_LEVEL {
                let leaf = self.leaf(visits[ptr])?;
                Ok((visits[ptr], Some(count_leaves(leaf))))
            } else {
                Ok((visits[ptr], Some(self.get(visits[ptr])? >> 8)))
            }
        } else if level == LEAF_LEVEL {
            let &right = pool.get(ptr + 1).ok_or(format!(
                "Least significant leaf mask index out of bounds! Leaf level, Pointer: {}",
                ptr + 1
            ))?;
            let leaf = &[pool[ptr], right];
            let node = match pool {
                Strict(_) => Strict(leaf),
                Pass(_) => Pass(leaf),
            };
            visits[ptr] = self.add_leaf(node, node.hash_as_leaf())?;
            Ok((visits[ptr], Some(count_leaves(leaf))))
        } else {
            let child_mask = pool[ptr] & 0xff;
            let children = (child_mask as u8).count_ones() as usize;
            let mut interior = Vec::with_capacity(children + 1);
            interior.push(child_mask);
            let voxel_count = if COLOR_TREE_LEVELS <= level {
                let mut voxel_count = 0;
                for &ptr in pool.iter().skip(ptr + 1).take(children) {
                    let (vptr, count) = self.put_no_seek(pool, level + 1, ptr as usize, visits)?;
                    interior.push(vptr);
                    voxel_count += count.unwrap();
                }
                interior[0] |= voxel_count << 8;
                Some(voxel_count)
            } else {
                for &ptr in pool.iter().skip(ptr + 1).take(children) {
                    let (vptr, _) = self.put_no_seek(pool, level + 1, ptr as usize, visits)?;
                    interior.push(vptr);
                }
                None
            };
            let node = match pool {
                Strict(_) => Strict(&interior),
                Pass(_) => Pass(&interior),
            };
            visits[ptr] = self.add_interior(level, node, node.hash_as_interior())?;
            Ok((visits[ptr], voxel_count))
        }
    }

    #[inline]
    fn no_seek(&mut self, levels: u32, pool: Node, root_idx: usize) -> Result<u32> {
        let root_level = SUPPORTED_LEVELS - levels;
        let mut visits = vec![!0; pool.len()].into_boxed_slice();
        let (vptr, _) = self.put_no_seek(pool, root_level, root_idx, &mut visits)?;
        Ok(vptr)
    }
}

fn main() {
    bench_staging();
    import_epiccitadel128k();
}

const CURRENT: bool = true;
const NAME: &str = "experimental";
fn import_epiccitadel128k<'shmem>() -> Option<(BasicHashDAG<'shmem>, u32)> {
    let time = SystemTime::now();
    println!("Loading data...");
    let file = format!("epiccitadel128k{}", DAG_SUFFIX);
    if let Some(bd) = BasicDAG::from_file(Path::new(SVDAG_STORE).join(file)) {
        assert_eq!(17, bd.levels);
        assert_eq!(bd.pool.len(), 257007622);
        for (idx, val) in load_ron::<Vec<(usize, u32)>>("data-points").unwrap() {
            assert_eq!(bd.pool[idx], val, "At: {}", idx);
        }
        println!("Job took {} seconds", time.elapsed().unwrap().as_secs());

        println!("Importing...");
        let time = SystemTime::now();
        const BYTES: usize = 2.5e9 as usize;
        std::fs::create_dir_all(FLINK_STORE).expect("Could not create directory.");
        let config = ShmemConfig {
            path: format!("{}{}-config.json", FLINK_STORE, NAME),
            class: stringify!(BasicHashDAG).into(),
        }
        .write()
        .expect("Failed to write the config file.");
        let root = Some(format!("{}{}-", FLINK_STORE, NAME));
        let mut dag = BasicHashDAG::with_capacity(root.as_ref(), BYTES / 4).unwrap();
        let vptr = if CURRENT {
            dag.import_strict(&bd, None).unwrap()
        } else {
            let BasicDAG { levels, pool, .. } = bd;
            dag.no_seek(levels, Strict(&pool), 0).unwrap()
        };
        assert_ne!(vptr, 0);
        dbg!(vptr);
        println!("Job took {} seconds", time.elapsed().unwrap().as_secs());
        println!(
            "Aggregated {} bytes of data",
            dag.staging_specs().total_size()
        );
        log::debug!("Wrote config: {:?}", config);
        Some((dag, vptr))
    } else {
        println!("Never mind... ZzZzZzZ...");
        None
    }
}

fn bench_staging() {
    fn add_lantern<T: Tracker>(dag: &mut SharedHashDAG<HashTable, T>) -> (Result<u32>, BasicDAG) {
        let bd = {
            let mut file = File::open("assets/lantern.comp.bin").unwrap();
            file.read_exact(&mut [0; 8 * 6]).unwrap();
            let levels = read_word(&mut file).unwrap();
            let num_nodes = read_word(&mut file).unwrap() as usize;
            file.read_exact(&mut [0; 4 * 6]).unwrap();
            let pool = read_exact_slice(&mut file, num_nodes).unwrap();
            BasicDAG::new(levels, pool)
        };
        (dag.import_strict(&bd, None), bd)
    }
    fn stage(dag: &mut BasicHashDAG, pool_dst: &mut [u32], lut_dst: &mut [u32]) {
        let specs = dag.staging_specs();
        let mut pool_src = vec![0; specs.pool_items as _].into_boxed_slice();
        let mut lut_src = vec![0; specs.pages as _].into_boxed_slice();
        let (pool, page_table) = dag.hash_dag.dump();
        // Create a staging buffer
        dag.stage(
            |src_range, dst_range| pool_src[src_range].copy_from_slice(&pool[dst_range]),
            |src_range, dst_range| lut_src[src_range].copy_from_slice(&page_table[dst_range]),
        );
        // Stage to device buffer
        dag.stage(
            |src_range, dst_range| pool_dst[dst_range].copy_from_slice(&pool_src[src_range]),
            |src_range, dst_range| lut_dst[dst_range].copy_from_slice(&lut_src[src_range]),
        );
    }

    let time = SystemTime::now();
    let mut avg_time_staging = 0.0;
    const ITER: u32 = 100;
    for _ in 0..ITER {
        let mut dag = BasicHashDAG::with_capacity(None, (32_000_000 / 4) as _).unwrap();
        let mut dev_pool = vec![0; dag.pool.len()].into_boxed_slice();
        let mut dev_lut = vec![0; TOTAL_PAGES as _].into_boxed_slice();
        // Import & stage
        add_lantern(&mut dag).0.unwrap();
        let time = SystemTime::now();
        stage(&mut dag, &mut dev_pool, &mut dev_lut);
        avg_time_staging += time.elapsed().unwrap().as_micros() as f64;
        dag.tracker.clear();
        assert_eq!(dev_pool.as_ref().cmp(&dag.pool), Ordering::Equal);
    }
    let avg_time_spent = time.elapsed().unwrap().as_micros() as f64 / ITER as f64;
    avg_time_staging /= ITER as f64;
    dbg!(avg_time_spent);
    dbg!(avg_time_staging);
    let avg_time_rest = avg_time_spent - avg_time_staging;
    dbg!(avg_time_rest);
}
