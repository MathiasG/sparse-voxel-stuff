use crate::{
    hash_dag::{
        editing::Operation::{Link, Unlink},
        utils::serialization::load_ron,
    },
    shared_xform::SharedTransform,
    utils::{new_eye_xform, new_model_xform},
    State, UserPreferences,
};
use ::{
    gilrs::{
        ev::{
            Axis,
            EventType::{AxisChanged, ButtonChanged},
        },
        Button, Event, Gilrs,
    },
    nalgebra::{UnitQuaternion, Vector3},
    std::{
        collections::HashMap,
        time::{Duration, SystemTime, UNIX_EPOCH},
    },
};

pub const REPEAT_DELAY: Duration = Duration::from_millis(100);

pub struct GamepadInputManager {
    gilrs: Gilrs,
    gamepad: gilrs::GamepadId,
    controls_eye: bool,
    btn_presses: HashMap<Button, SystemTime>,
}

impl GamepadInputManager {
    #[inline]
    pub fn new(gilrs: Gilrs, gamepad: gilrs::GamepadId) -> Self {
        Self {
            gilrs,
            gamepad,
            controls_eye: true,
            btn_presses: HashMap::new(),
        }
    }

    fn cache_events(&mut self, pref: &UserPreferences) {
        while let Some(event) = self.gilrs.next_event() {
            if event.id == self.gamepad {
                match event.event {
                    AxisChanged(axis, mov, code) => {
                        if pref.deadzone < mov.abs() {
                            self.gilrs.update(&event)
                        } else {
                            let event = Event::new(self.gamepad, AxisChanged(axis, 0.0, code));
                            self.gilrs.update(&event)
                        }
                    }
                    ButtonChanged(_, _, _) => self.gilrs.update(&event),
                    _ => {}
                };
            }
        }
    }

    fn control(
        &self,
        dt: f32,
        pref: &UserPreferences,
        sticks: &[f32; 4],
        xform: &mut SharedTransform,
    ) {
        if sticks[0] != 0.0 {
            let mov = dt * sticks[0] * pref.mov_sens_x;
            let translation = xform.rot() * Vector3::x() * mov;
            xform.translation.vector += translation;
        }
        if sticks[1] != 0.0 {
            let mov = dt * sticks[1] * pref.mov_sens_z;
            let translation = xform.rot() * Vector3::z() * mov;
            xform.translation.vector += translation;
        }
        if sticks[2] != 0.0 {
            let mov = dt * sticks[2] * pref.look_sens_y;
            let axisangle = if pref.lock_to_xz {
                Vector3::y() * mov
            } else {
                xform.rot() * Vector3::y() * mov
            };
            let rotation = UnitQuaternion::new(axisangle);
            xform.append_rotation_wrt_center_mut(&rotation);
        }
        if sticks[3] != 0.0 {
            let mov = dt * sticks[3] * pref.look_sens_x;
            let axisangle = xform.rot() * -Vector3::x() * mov;
            let rotation = UnitQuaternion::new(axisangle);
            xform.append_rotation_wrt_center_mut(&rotation);
        }
    }

    pub fn process_events(
        &mut self,
        dt: Duration,
        pref: &mut UserPreferences,
        s: &mut State,
    ) -> Option<()> {
        self.cache_events(pref);
        let gamepad = self.gilrs.connected_gamepad(self.gamepad)?;
        let sticks = [
            gamepad.value(Axis::LeftStickX),
            gamepad.value(Axis::LeftStickY),
            gamepad.value(Axis::RightStickX),
            gamepad.value(Axis::RightStickY),
        ];
        let xform = if self.controls_eye {
            &mut s.eye
        } else {
            &mut s.models[0].xform
        };
        self.control(dt.as_secs_f32(), pref, &sticks, xform);
        if let Some(btn) = gamepad.button_data(Button::Mode) {
            if btn.is_pressed() {
                let ts = self.btn_presses.get(&Button::Mode).or(Some(&UNIX_EPOCH));
                if let Ok(dt) = btn.timestamp().duration_since(*ts.unwrap()) {
                    if REPEAT_DELAY < dt {
                        log::debug!("Resetting user preferences.");
                        *pref = load_ron::<UserPreferences>("user-pref").unwrap();
                        self.btn_presses.insert(Button::Mode, btn.timestamp());
                    }
                }
            }
        }
        if let Some(btn) = gamepad.button_data(Button::Start) {
            if btn.is_pressed() {
                let ts = self.btn_presses.get(&Button::Start).or(Some(&UNIX_EPOCH));
                if let Ok(dt) = btn.timestamp().duration_since(*ts.unwrap()) {
                    if REPEAT_DELAY < dt {
                        let subject = if self.controls_eye {
                            s.eye = new_eye_xform(pref).into();
                            "eye"
                        } else {
                            s.models[0].xform = new_model_xform(s.models[0].levels()).into();
                            "models[0]"
                        };
                        log::debug!("Position and orientation of {} is reset.", subject);
                        self.btn_presses.insert(Button::Start, btn.timestamp());
                    }
                }
            }
        }
        if let Some(btn) = gamepad.button_data(Button::South) {
            if btn.is_pressed() {
                let ts = self.btn_presses.get(&Button::South).or(Some(&UNIX_EPOCH));
                if btn.timestamp().duration_since(*ts.unwrap()).is_ok() {
                    if s.target.x != !0 {
                        if super::CAPTURE_EDIT {
                            if let Some(ref mut rd) = s.rd {
                                rd.trigger_capture();
                            }
                        }
                        s.edit = Some((Link, s.target));
                    }
                    self.btn_presses.insert(Button::South, btn.timestamp());
                }
            }
        }
        if let Some(btn) = gamepad.button_data(Button::East) {
            if btn.is_pressed() {
                let ts = self.btn_presses.get(&Button::East).or(Some(&UNIX_EPOCH));
                if btn.timestamp().duration_since(*ts.unwrap()).is_ok() {
                    if s.target.x != !0 {
                        if super::CAPTURE_EDIT {
                            if let Some(ref mut rd) = s.rd {
                                rd.trigger_capture();
                            }
                        }
                        s.edit = Some((Unlink, s.target));
                    }
                    self.btn_presses.insert(Button::East, btn.timestamp());
                }
            }
        }
        if let Some(btn) = gamepad.button_data(Button::Select) {
            if btn.is_pressed() {
                let ts = self.btn_presses.get(&Button::Select).or(Some(&UNIX_EPOCH));
                if let Ok(dt) = btn.timestamp().duration_since(*ts.unwrap()) {
                    if REPEAT_DELAY < dt {
                        self.controls_eye = !self.controls_eye;
                        self.btn_presses.insert(Button::Select, btn.timestamp());
                    }
                }
            }
        }
        Some(())
    }
}
