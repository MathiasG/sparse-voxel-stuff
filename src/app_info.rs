#![allow(dead_code)]

pub const NAME: &str = "Sparse Voxels";
pub const SVDAG_STORE: &str = ".local/svdags/";
pub const FLINK_STORE: &str = ".shm/";
pub const DAG_SUFFIX: &str = ".basic_dag.dag.bin";
pub const CAPTURES_PATH: &str = ".captures/";