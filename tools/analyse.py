#!/usr/bin/python3

from analytics import toolkit
from analytics.constants import SHMEM_STORE, CLASS, CONFIG
from analytics.utils import load_state, fail

import matplotlib.pyplot as plt
from os.path import normpath
import sys
import json
import getopt

# Import for argument parsing (-a)
import numpy as np

# Something of note (about security): eval is used for parsing text.

'''
Analytics CLI.
Don't judge, really simple, only cared about getting the job done.
(Why big words when small does trick?)
'''


def print_help_exit():
    print(
        f'{sys.argv[0]} -n <target> -t <tool> [-a "arg1=val1, (...), argn=valn", -p <path>]'
    )
    sys.exit()


def non_existent_tool(CLASS, **_):
    print(f'The tool you look for does not exist.\n')
    print(f'The {CLASS} toolkit contains:')
    for tool in toolkit.toolkit[CLASS]:
        print(f' * {tool}: {toolkit.toolkit[CLASS][tool].__doc__}')
    print(f'\nIf you wish to query the full toolkit run tools/query_toolkit.py')


def analyse(path, name, tool_name, args):
    state = load_state(path, name)
    state.update(args)
    state[CLASS] = state[CONFIG][CLASS]
    print(f'Running "{tool_name}" from the {state[CLASS]} toolkit...\n')
    toolkit.run(tool_name, state, non_existent_tool)


def from_cli(argv):
    plt.style.use('dark_background')
    try:
        opts, _ = getopt.getopt(
            argv, 'hn:p:t:a:',
            ['name=', 'path=', 'tool=', 'arguments=']
        )
    except getopt.GetoptError:
        print_help_exit()

    path, name, tool, args = SHMEM_STORE, None, 'print-config', {}
    for opt, arg in opts:
        if opt == '-h':
            print_help_exit()
        elif opt in ('-n', '--name'):
            name = arg
        elif opt in ('-p', '--path'):
            path = arg
        elif opt in ('-t', '--tool'):
            tool = arg
        elif opt in ('-a', '--arguments'):
            for arg in arg.split(','):
                arg = arg.split('=')
                if len(arg) != 2:
                    print('Check "--argument" or "-a"')
                    print_help_exit()
                try:
                    args[arg[0].replace(' ', '')] = eval(arg[1])
                except:
                    fail(f'Something went wrong parsing "{arg[1]}".')
    if name:
        analyse(path, name, tool, args)
    else:
        print_help_exit()


if __name__ == '__main__':
    from_cli(sys.argv[1:])
