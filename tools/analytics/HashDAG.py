def new_bucket_len_idx(level, BUCKETS_PER_HI_LEVEL, TOTAL_HI_BUCKETS, HI_LEVELS, BUCKETS_PER_LO_LEVEL, bucket=0, **_):
    return level * BUCKETS_PER_HI_LEVEL if level < HI_LEVELS else TOTAL_HI_BUCKETS + (level - HI_LEVELS) * BUCKETS_PER_LO_LEVEL + bucket


def buckets_per_level(level, BUCKETS_PER_HI_LEVEL, BUCKETS_PER_LO_LEVEL, HI_LEVELS, **_):
    return BUCKETS_PER_HI_LEVEL if level < HI_LEVELS else BUCKETS_PER_LO_LEVEL


def new_bucket_len(level, HI_BUCKET_LEN, LO_BUCKET_LEN, HI_LEVELS, **_):
    return HI_BUCKET_LEN if level < HI_LEVELS else LO_BUCKET_LEN


def is_color_tree_level(level, COLOR_TREE_LEVELS, **_):
    return COLOR_TREE_LEVELS <= level


def is_last_interior(level, LEAF_LEVEL, **_):
    return LEAF_LEVEL == level + 1


def hi_lo_levels(HI_LEVELS, LEAF_LEVEL, **_):
    return (
        [l for l in range(0, HI_LEVELS)],
        [l for l in range(HI_LEVELS, LEAF_LEVEL + 1)]
    )


def general_stats(config, hash_dag, **_):
    '''Statistics not specific to any implementation.'''
    from analytics.utils import collect_stats
    print('General stats:')
    for k, v in collect_stats(config, hash_dag).items():
        if k == 'relative_size':
            print(f'* {k.replace("_", " ").title()}: {round(v*100, 2)}%')
        elif k == 'pool_bytes':
            print(f'* {k.replace("_", " ").title()}: {round(v/1e6, 2)} MB')
        else:
            print(f'* {k.replace("_", " ").title()}: {v}')


def print_config(config, **_):
    '''Print the loaded configuration JSON.'''
    import json
    print(f'Configuration = {json.dumps(config, indent=1)}')
