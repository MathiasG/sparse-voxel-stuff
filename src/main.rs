use self::{
    cpu_rt::{RTCache, RayTracer},
    game_state::*,
    hash_dag::{
        editing::{
            shapes::Sphere,
            Editor,
            Operation::{Link, Unlink},
            Shape,
        },
        hash_table::basic::HashTable,
        tracking::Tracker,
        utils::serialization::load_ron,
        HashDAG,
    },
    model::Model,
    renderer::{bindings::*, buffer::Buffer, context::VkContext, Renderer, LOCAL},
    shared_xform::SharedTransform,
    utils::{any_as_u8_slice, new_child_lut, new_eye_xform},
};
use ::{
    ash::{extensions::khr::Surface, version::DeviceV1_0, vk, Device, Entry},
    core::fmt::Debug,
    gilrs::Gilrs,
    nalgebra::{UnitQuaternion, Vector3},
    serde_derive::*,
    simplelog::{CombinedLogger, Config, TermLogger, TerminalMode, WriteLogger},
    std::{fs::File, mem::size_of, time::SystemTime},
    winit::{
        dpi::{LogicalSize, PhysicalSize},
        event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::{Fullscreen, WindowBuilder},
    },
};

mod app_info;
mod cpu_rt;
mod game_state;
mod gamepad;
mod hash_dag;
mod model;
mod renderer;
mod shared_xform;
mod utils;

const CAPTURE_EDIT: bool = false;
const DUPLICATE: bool = false;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct UserPreferences {
    vram: usize,
    width: u16,
    height: u16,
    fov: f32,
    aperture: f32,
    benchmark: bool,
    start_pos_eye: [f32; 3],
    fullscreen: bool,
    demo: u8,
    look_sens_y: f32,
    look_sens_x: f32,
    mov_sens_z: f32,
    mov_sens_x: f32,
    deadzone: f32,
    lock_to_xz: bool,
    load_renderdoc: bool,
}

impl UserPreferences {
    #[inline]
    fn pool_mem(&self) -> usize {
        self.vram / size_of::<u32>()
    }
}

pub mod trace {
    #[repr(C, align(16))]
    #[derive(Debug)]
    pub struct PushConstants {
        pub ray: [[f32; 4]; 4],
        pub target: [u32; 3],
        pub time: f32,
    }

    #[repr(C, align(16))]
    #[derive(Debug)]
    pub struct Model {
        pub xform: [[f32; 4]; 4],
        pub vptr: u32,
        pub level: u32,
    }
}

impl Renderer {
    fn new<DAG: HashDAG>(
        pref: &UserPreferences,
        s: &mut State,
        event_loop: &EventLoop<()>,
    ) -> Self {
        log::debug!("Creating a renderer...");

        let &UserPreferences {
            width,
            height,
            fullscreen,
            fov,
            aperture,
            ..
        } = pref;

        let window = WindowBuilder::new()
            .with_title(app_info::NAME)
            .with_inner_size(LogicalSize::new(width, height))
            .build(event_loop)
            .unwrap();
        window.set_cursor_visible(false);
        if fullscreen {
            window.set_fullscreen(Some(Fullscreen::Borderless(window.current_monitor())));
        }

        let entry = Entry::new().expect("Failed to create entry.");
        let instance = Self::new_instance(&entry, &window);
        let surface = Surface::new(&entry, &instance);
        let surface_khr =
            unsafe { ash_window::create_surface(&entry, &instance, &window, None).unwrap() };

        let debug_report_callback = renderer::debug::setup_debug_messenger(&entry, &instance);
        let (physical_device, queue_families_indices) =
            Self::lets_get_physical(&instance, &surface, surface_khr);
        let (device, compute_queue, present_queue) =
            Self::create_logical_device(&instance, physical_device, queue_families_indices);
        log::debug!("Selected a logical device.");
        let vk_context = VkContext::new(
            entry,
            instance,
            debug_report_callback,
            surface,
            surface_khr,
            physical_device,
            device,
        );

        let device = vk_context.device();
        let [width, height]: [u32; 2] = window.inner_size().into();
        let extent = vk::Extent2D { width, height };
        let compute_idx = queue_families_indices.compute_index;

        let (swapchain, swapchain_khr, swapchain_properties, swapchain_images) =
            Self::new_swapchain(&vk_context, queue_families_indices, extent);
        log::debug!("Built the swapchain with images.");
        let swapchain_image_views =
            Self::new_image_views(device, &swapchain_images, swapchain_properties);
        log::debug!("Created swapchain image views.");
        let descriptor_pool = Self::create_descriptor_pool(device);
        let descriptor_set_layout = Self::new_descriptor_set_layout(device);
        let descriptor_sets = unsafe {
            let allocation = vk::DescriptorSetAllocateInfo::builder()
                .descriptor_pool(descriptor_pool)
                .set_layouts(&[descriptor_set_layout])
                .build();
            device.allocate_descriptor_sets(&allocation).unwrap()
        };
        log::debug!("Allocated a descriptor set.");
        let (pipeline, pipeline_layout) = Self::new_pipeline(
            device,
            descriptor_set_layout,
            &s.dag,
            (extent, pref.fov, pref.aperture),
        );
        log::debug!("Created the pipeline (+ layout).");

        let command_pool = Self::new_command_pool(
            device,
            compute_idx,
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        );
        log::debug!("Created a command pool.");
        // Self::create_compute_command_buffer
        let command_buffers = {
            let alloc_info = vk::CommandBufferAllocateInfo::builder()
                .command_pool(command_pool)
                .level(vk::CommandBufferLevel::PRIMARY)
                .command_buffer_count(1)
                .build();
            unsafe { device.allocate_command_buffers(&alloc_info) }.unwrap()
        };
        log::debug!("Created command buffers.");
        // Self::create_compute_fence
        let fence = {
            let info = vk::FenceCreateInfo::builder()
                .flags(vk::FenceCreateFlags::SIGNALED)
                .build();
            unsafe { device.create_fence(&info, None) }.unwrap()
        };
        // Self::create_semaphores
        let (image_available, render_finished) = {
            let create_info = vk::SemaphoreCreateInfo::builder().build();
            let image_available_semaphore =
                unsafe { device.create_semaphore(&create_info, None) }.unwrap();
            let render_finished_semaphore =
                unsafe { device.create_semaphore(&create_info, None) }.unwrap();
            (image_available_semaphore, render_finished_semaphore)
        };
        log::debug!("Created a fence and semaphores.");
        // Create buffers
        let transient_command_pool =
            Self::new_command_pool(device, compute_idx, vk::CommandPoolCreateFlags::TRANSIENT);
        log::debug!("Created a transient command pool.");
        let param = (&vk_context, transient_command_pool, compute_queue);
        let static_buf = Self::new_dev_loc_buf_with_data(
            param,
            vk::BufferUsageFlags::STORAGE_BUFFER,
            &new_child_lut(),
        );
        let (pool, page_table) = s.dag.hash_dag.dump();
        let (pool_buf, page_table_buf) = {
            if DUPLICATE {
                s.dag.tracker.clear();
                let buf = Self::new_dev_loc_buf_with_data;
                (
                    buf(param, vk::BufferUsageFlags::STORAGE_BUFFER, pool),
                    buf(param, vk::BufferUsageFlags::STORAGE_BUFFER, page_table),
                )
            } else {
                let buf = |data: &[u32]| unsafe {
                    vk_context.new_dev_loc_buf(
                        (data.len() * size_of::<u32>()) as _,
                        vk::BufferUsageFlags::STORAGE_BUFFER,
                    )
                };
                (buf(pool), buf(page_table))
            }
        };
        let model_buf: Buffer = Self::new_model_buffer(param, &mut s.models[0]);
        unsafe { device.destroy_command_pool(transient_command_pool, None) };
        log::debug!("Allocated and populated buffers.");

        let mut renderer = Self {
            vk_context,
            queue_families_indices,
            compute_queue,
            present_queue,
            swapchain,
            swapchain_khr,
            swapchain_properties,
            image_idx: 0,
            swapchain_images,
            swapchain_image_views,
            descriptor_set_layout,
            pipeline_layout,
            pipeline,
            command_pool,
            pool_buf,
            page_table_buf,
            static_buf,
            model_buf,
            descriptor_pool,
            descriptor_sets,
            command_buffers,
            image_available,
            render_finished,
            fence,
            rt_cache: RTCache::new(&s.models[0], s.eye.rot() * Vector3::z()),
            resize: None,
            fov,
            aperture,
            window,
        }
        .initialize_descriptor_set();
        renderer.stage_hash_dag_changes(&mut s.dag);
        renderer
    }

    fn new_descriptor_set_layout(device: &Device) -> vk::DescriptorSetLayout {
        let bindings = [
            vk::DescriptorSetLayoutBinding::builder()
                .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
                .stage_flags(vk::ShaderStageFlags::COMPUTE)
                .descriptor_count(1)
                .binding(RENDER)
                .build(),
            vk::DescriptorSetLayoutBinding::builder()
                .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                .stage_flags(vk::ShaderStageFlags::COMPUTE)
                .descriptor_count(1)
                .binding(POOL)
                .build(),
            vk::DescriptorSetLayoutBinding::builder()
                .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                .stage_flags(vk::ShaderStageFlags::COMPUTE)
                .descriptor_count(1)
                .binding(PAGE_LUT)
                .build(),
            vk::DescriptorSetLayoutBinding::builder()
                .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                .stage_flags(vk::ShaderStageFlags::COMPUTE)
                .descriptor_count(1)
                .binding(STATIC)
                .build(),
            vk::DescriptorSetLayoutBinding::builder()
                .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
                .stage_flags(vk::ShaderStageFlags::COMPUTE)
                .descriptor_count(1)
                .binding(MODEL)
                .build(),
        ];
        let create_info = vk::DescriptorSetLayoutCreateInfo::builder()
            .bindings(&bindings)
            .build();
        let layout = unsafe { device.create_descriptor_set_layout(&create_info, None) }.unwrap();
        log::debug!("Created a descriptor set layout.");
        layout
    }

    fn initialize_descriptor_set(self) -> Self {
        let descriptor_writes = [
            vk::WriteDescriptorSet::builder()
                .dst_set(self.descriptor_sets[0])
                .dst_binding(POOL)
                .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                .buffer_info(&[vk::DescriptorBufferInfo {
                    buffer: self.pool_buf.buffer,
                    offset: 0,
                    range: self.pool_buf.size,
                }])
                .build(),
            vk::WriteDescriptorSet::builder()
                .dst_set(self.descriptor_sets[0])
                .dst_binding(PAGE_LUT)
                .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                .buffer_info(&[vk::DescriptorBufferInfo {
                    buffer: self.page_table_buf.buffer,
                    offset: 0,
                    range: self.page_table_buf.size,
                }])
                .build(),
            vk::WriteDescriptorSet::builder()
                .dst_set(self.descriptor_sets[0])
                .dst_binding(STATIC)
                .descriptor_type(vk::DescriptorType::STORAGE_BUFFER)
                .buffer_info(&[vk::DescriptorBufferInfo {
                    buffer: self.static_buf.buffer,
                    offset: 0,
                    range: self.static_buf.size,
                }])
                .build(),
            vk::WriteDescriptorSet::builder()
                .dst_set(self.descriptor_sets[0])
                .dst_binding(MODEL)
                .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
                .buffer_info(&[vk::DescriptorBufferInfo {
                    buffer: self.model_buf.buffer,
                    offset: 0,
                    range: size_of::<trace::Model>() as _,
                }])
                .build(),
        ];
        let device = self.vk_context.device();
        unsafe { device.update_descriptor_sets(&descriptor_writes, &[]) }
        log::debug!("Initialized the descriptor set.");
        self
    }

    fn record_command_buffer(&mut self, s: &mut State) {
        let device = self.vk_context.device();
        let command_buffer = self.command_buffers[0];
        let swapchain_image = self.swapchain_images[self.image_idx];
        let vk::Extent2D { width, height } = self.swapchain_properties.extent;
        let [group_count_x, group_count_y] = [
            (width + LOCAL[0] - 1) / LOCAL[0],
            (height + LOCAL[1] - 1) / LOCAL[1],
        ];

        let begin_info = vk::CommandBufferBeginInfo::builder()
            .flags(vk::CommandBufferUsageFlags::SIMULTANEOUS_USE)
            .build();
        let (ray, target, time) = {
            let (dag, model) = (&s.dag.hash_dag, &s.models[0]);
            self.rt_cache.renew(model, s.eye.rot() * Vector3::z());
            let mut rt = RayTracer::new(&s.eye, model, &mut self.rt_cache);
            s.target = rt.trace(dag, 500).unwrap().path;
            (
                (*s.eye.updated_matrix()).into(),
                s.target.into(),
                s.ts.elapsed().unwrap().as_secs_f32(),
            )
        };
        unsafe {
            device
                .begin_command_buffer(command_buffer, &begin_info)
                .unwrap();
            device.cmd_push_constants(
                command_buffer,
                self.pipeline_layout,
                vk::ShaderStageFlags::COMPUTE,
                0,
                any_as_u8_slice(&trace::PushConstants { ray, target, time }),
            );
            device.cmd_bind_pipeline(
                command_buffer,
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline,
            );
            device.cmd_bind_descriptor_sets(
                command_buffer,
                vk::PipelineBindPoint::COMPUTE,
                self.pipeline_layout,
                0,
                &self.descriptor_sets,
                &[],
            );
        };
        unsafe {
            device.cmd_pipeline_barrier(
                command_buffer,
                vk::PipelineStageFlags::TOP_OF_PIPE,
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::DependencyFlags::default(),
                &[],
                &[],
                &[vk::ImageMemoryBarrier::builder()
                    .image(swapchain_image)
                    .old_layout(vk::ImageLayout::UNDEFINED)
                    .new_layout(vk::ImageLayout::GENERAL)
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    })
                    .src_access_mask(vk::AccessFlags::default())
                    .dst_access_mask(vk::AccessFlags::SHADER_WRITE)
                    .build()],
            )
        };
        unsafe { device.cmd_dispatch(command_buffer, group_count_x, group_count_y, 1) };
        unsafe {
            device.cmd_pipeline_barrier(
                command_buffer,
                vk::PipelineStageFlags::COMPUTE_SHADER,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                vk::DependencyFlags::default(),
                &[],
                &[],
                &[vk::ImageMemoryBarrier::builder()
                    .image(swapchain_image)
                    .old_layout(vk::ImageLayout::GENERAL)
                    .new_layout(vk::ImageLayout::PRESENT_SRC_KHR)
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    })
                    .src_access_mask(vk::AccessFlags::SHADER_WRITE)
                    .dst_access_mask(vk::AccessFlags::MEMORY_READ)
                    .build()],
            )
        };
        unsafe { device.end_command_buffer(command_buffer) }.unwrap();
    }

    fn update_desc_sets(&mut self) {
        let writes = [
            vk::WriteDescriptorSet::builder()
                .dst_set(self.descriptor_sets[0])
                .dst_binding(RENDER)
                .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
                .image_info(&[vk::DescriptorImageInfo {
                    sampler: vk::Sampler::default(),
                    image_view: self.swapchain_image_views[self.image_idx],
                    image_layout: vk::ImageLayout::GENERAL,
                }])
                .build(),
            vk::WriteDescriptorSet::builder()
                .dst_set(self.descriptor_sets[0])
                .dst_binding(MODEL)
                .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
                .buffer_info(&[vk::DescriptorBufferInfo {
                    buffer: self.model_buf.buffer,
                    offset: 0,
                    range: size_of::<trace::Model>() as _,
                }])
                .build(),
        ];
        let device = self.vk_context.device();
        unsafe { device.update_descriptor_sets(&writes, &[]) }
    }

    #[inline]
    fn new_model_buffer(
        param: (&VkContext, vk::CommandPool, vk::Queue),
        model: &mut Model,
    ) -> Buffer {
        Self::new_dev_loc_buf_with_data(param, vk::BufferUsageFlags::UNIFORM_BUFFER, unsafe {
            any_as_u8_slice(&trace::Model {
                xform: (*model.xform.updated_matrix()).into(),
                vptr: model.vptr,
                level: model.level(),
            })
        })
    }

    #[inline]
    fn renew_model_buffer(&mut self, model: &mut Model) {
        log::trace!("Renewing the model buffer");
        unsafe { self.model_buf.drop(self.vk_context.device()) };
        self.model_buf = Self::new_model_buffer(
            (&self.vk_context, self.command_pool, self.compute_queue),
            model,
        );
    }

    fn renew_swapchain(&mut self, s: &mut State) {
        log::debug!("Recreating swapchain.");
        self.wait_gpu_idle();
        self.drop_swapchain();
        let extent = self.resize.unwrap_or(self.swapchain_properties.extent);
        self.resize = None;

        let (swapchain, swapchain_khr, properties, images) =
            Self::new_swapchain(&self.vk_context, self.queue_families_indices, extent);
        self.swapchain = swapchain;
        self.swapchain_khr = swapchain_khr;
        self.swapchain_properties = properties;
        self.swapchain_images = images;

        let device = self.vk_context.device();
        self.swapchain_image_views =
            Self::new_image_views(device, &self.swapchain_images, self.swapchain_properties);

        let (pipeline, layout) = Self::new_pipeline(
            device,
            self.descriptor_set_layout,
            &s.dag,
            (self.swapchain_properties.extent, self.fov, self.aperture),
        );
        self.pipeline = pipeline;
        self.pipeline_layout = layout;

        self.command_buffers = unsafe {
            device.allocate_command_buffers(
                &vk::CommandBufferAllocateInfo::builder()
                    .command_pool(self.command_pool)
                    .level(vk::CommandBufferLevel::PRIMARY)
                    .command_buffer_count(1)
                    .build(),
            )
        }
        .unwrap();
        self.update_desc_sets();
        self.record_command_buffer(s);
        self.resize = None;
    }

    fn draw(&mut self, s: &mut State) -> bool {
        {
            let device = self.vk_context.device();
            unsafe { device.wait_for_fences(&[self.fence], true, !0) }.unwrap();
        }

        self.image_idx = match unsafe {
            self.swapchain.acquire_next_image(
                self.swapchain_khr,
                500_000_000,
                self.image_available,
                vk::Fence::null(),
            )
        } {
            Ok((image_idx, _)) => image_idx as usize,
            Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                return true;
            }
            Err(error) => panic!("Error while acquiring next image. Cause: {}", error),
        };

        {
            let device = self.vk_context.device();
            unsafe { device.reset_fences(&[self.fence]) }.unwrap();
        }

        self.stage_hash_dag_changes(&mut s.dag);
        {
            let model = &mut s.models[0];
            if model.changed || model.xform.needs_update() {
                log::debug!(
                    "Reason for model buffer invalidation: vptr={}, xform={}",
                    model.changed,
                    model.xform.needs_update()
                );
                model.changed = false;
                self.renew_model_buffer(model);
            }
        }
        self.update_desc_sets();
        self.record_command_buffer(s);
        {
            let device = self.vk_context.device();
            let submit_info = vk::SubmitInfo::builder()
                .command_buffers(&self.command_buffers)
                .wait_semaphores(&[self.image_available])
                .signal_semaphores(&[self.render_finished])
                .wait_dst_stage_mask(&[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
                .build();
            unsafe { device.queue_submit(self.compute_queue, &[submit_info], self.fence) }.unwrap();
        }

        let present_info = vk::PresentInfoKHR::builder()
            .wait_semaphores(&[self.render_finished])
            .swapchains(&[self.swapchain_khr])
            .image_indices(&[self.image_idx as u32])
            .build();
        match unsafe {
            self.swapchain
                .queue_present(self.present_queue, &present_info)
        } {
            Ok(true) | Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => true,
            Err(error) => panic!("Failed to present queue. Cause: {}", error),
            _ => false,
        }
    }
}

fn main() {
    let suggested_log_level = env_logger::Logger::from_default_env().filter();
    let writer = File::create("sparse-voxels.log").unwrap();
    CombinedLogger::init(vec![
        TermLogger::new(suggested_log_level, Config::default(), TerminalMode::Mixed),
        WriteLogger::new(suggested_log_level, Config::default(), writer),
    ])
    .unwrap();
    let mut pref = load_ron::<UserPreferences>("user-pref").unwrap();
    let event_loop = EventLoop::new();
    let mut s = GameState::new(&pref);
    let (mut gamepad, mut is_connected) = {
        let gilrs = Gilrs::new().expect("Trouble with Gilrs");
        let gamepad = if let Some((id, gamepad)) = gilrs.gamepads().next() {
            log::debug!("Selected gamepad: {}", gamepad.name());
            Some(gamepad::GamepadInputManager::new(gilrs, id))
        } else {
            None
        };
        (gamepad, true)
    };
    let mut renderer = Renderer::new::<HashTable>(&pref, &mut s, &event_loop);
    log::debug!("The renderer has been created.");
    let (mut is_swapchain_invalid, mut frame_ts) = (false, SystemTime::now());
    let (mut frames_ts, mut frames) = (SystemTime::now(), 0);
    event_loop.run(move |event, _, control_flow| match event {
        Event::LoopDestroyed => renderer.wait_gpu_idle(),
        Event::MainEventsCleared => {
            if (0, 0) != renderer.window.inner_size().into() {
                {
                    if let Some((op, edit)) = s.edit.take() {
                        let time = SystemTime::now();
                        const R: u32 = 50;
                        log::info!("Sphere({:?}, r={})", *edit, R);
                        let m = &mut s.models[0];
                        m.update(s.dag.edit(m.vptr, op, &Sphere::new(&edit, R)).unwrap());
                        let dt = time.elapsed().unwrap().as_millis();
                        log::debug!("Spent {}ms on editing.", dt);
                    }
                    let dt = frame_ts.elapsed().unwrap();
                    if let Some(ref mut gamepad) = gamepad {
                        is_connected = if gamepad.process_events(dt, &mut pref, &mut s).is_none() {
                            if is_connected {
                                log::warn!("The selected gamepad has lost connection.");
                            }
                            false
                        } else {
                            if !is_connected {
                                log::info!("The selected gamepad has been reconnected.");
                            }
                            true
                        };
                    }
                    let dt_input = frame_ts.elapsed().unwrap();
                    let dt = dt_input - dt;
                    if 5 < dt.as_millis() {
                        log::warn!("Input handling took {}ms!", dt.as_millis());
                    }
                }
                frame_ts = SystemTime::now();
                {
                    if is_swapchain_invalid {
                        renderer.renew_swapchain(&mut s);
                    }
                    is_swapchain_invalid = renderer.draw(&mut s);
                }
                if pref.benchmark {
                    frames += 1;
                    let dt = frames_ts.elapsed().unwrap();
                    if 2 < dt.as_secs() {
                        log::info!(
                            "Spent about {}ms on average on a frame.",
                            dt.as_millis() / frames
                        );
                        frames_ts = SystemTime::now();
                        frames = 0;
                    }
                }
            }
        }
        Event::DeviceEvent {
            event: winit::event::DeviceEvent::MouseMotion { delta },
            ..
        } => {
            {
                let mov = 0.001 * delta.0 as f32 * pref.look_sens_x;
                let axisangle = if pref.lock_to_xz {
                    Vector3::y() * mov
                } else {
                    s.eye.rot() * Vector3::y() * mov
                };
                let rotation = UnitQuaternion::new(axisangle);
                s.eye.append_rotation_wrt_center_mut(&rotation);
            }
            {
                let mov = 0.001 * delta.1 as f32 * pref.look_sens_x;
                let axisangle = s.eye.rot() * Vector3::x() * mov;
                let rotation = UnitQuaternion::new(axisangle);
                s.eye.append_rotation_wrt_center_mut(&rotation);
            }
        }
        Event::WindowEvent { event, .. } => match event {
            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
            WindowEvent::Resized(PhysicalSize { width, height }) => {
                log::debug!("Requested resize.");
                renderer.resize = Some(vk::Extent2D { width, height })
            }
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        virtual_keycode: Some(virtual_keycode),
                        state: ElementState::Pressed,
                        ..
                    },
                ..
            } => match virtual_keycode {
                VirtualKeyCode::F => match renderer.window.fullscreen() {
                    Some(_) => renderer.window.set_fullscreen(None),
                    None => renderer.window.set_fullscreen(Some(Fullscreen::Borderless(
                        renderer.window.current_monitor(),
                    ))),
                },
                VirtualKeyCode::F5 => {
                    log::debug!("Resetting user preferences.");
                    pref = load_ron::<UserPreferences>("user-pref").unwrap();
                }
                VirtualKeyCode::Space => {
                    s.eye = new_eye_xform(&pref).into();
                    log::debug!("Position and orientation of eye is reset.");
                }
                VirtualKeyCode::Numpad1 => translate_x(&mut s.eye, -0.05 * pref.mov_sens_x),
                VirtualKeyCode::Numpad3 => translate_x(&mut s.eye, 0.05 * pref.mov_sens_x),
                VirtualKeyCode::Numpad5 => translate_y(&mut s.eye, 0.05 * pref.mov_sens_z),
                VirtualKeyCode::Numpad2 => translate_y(&mut s.eye, -0.05 * pref.mov_sens_z),
                VirtualKeyCode::NumpadAdd | VirtualKeyCode::NumpadSubtract => {
                    if s.target.x != !0 {
                        if CAPTURE_EDIT {
                            if let Some(ref mut rd) = s.rd {
                                rd.trigger_capture();
                            }
                        }
                        let op = if virtual_keycode == VirtualKeyCode::NumpadAdd {
                            Link
                        } else {
                            Unlink
                        };
                        s.edit = Some((op, s.target));
                    }
                }
                _ => {}
            },
            _ => {}
        },
        _ => {}
    });
}

#[inline]
fn translate_x(xform: &mut SharedTransform, mov: f32) {
    let translation = xform.rot() * Vector3::x() * mov;
    xform.translation.vector += translation;
}

#[inline]
fn translate_y(xform: &mut SharedTransform, mov: f32) {
    let translation = xform.rot() * Vector3::z() * mov;
    xform.translation.vector += translation;
}
