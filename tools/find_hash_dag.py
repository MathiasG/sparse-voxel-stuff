#!/usr/bin/python3

from analytics.constants import SHMEM_STORE
from analytics.utils import is_valid_hash_dag, file_links

from os import listdir
from os.path import normpath

'''Finds active HashDAGs in the store (including leaked ones).'''


def main():
    print('Make sure to run this from the project root directory!\n')
    items = [normpath(f'{SHMEM_STORE}/{item}')
             for item in listdir(SHMEM_STORE)]
    available = is_valid_hash_dag(file_links(items))[True]
    print(f'Listing {len(available)} available HashDAG(s):')
    for flink in available:
        print('*', flink)


if __name__ == '__main__':
    main()
