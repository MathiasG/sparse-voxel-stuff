use super::buffer::Buffer;
use ash::{
    extensions::{ext::DebugReport, khr::Surface},
    version::{DeviceV1_0, InstanceV1_0},
    vk, Device, Entry, Instance,
};

pub struct VkContext {
    _entry: Entry,
    instance: Instance,
    debug_report_callback: Option<(DebugReport, vk::DebugReportCallbackEXT)>,
    surface: Surface,
    surface_khr: vk::SurfaceKHR,
    physical_device: vk::PhysicalDevice,
    device: Device,
}

impl VkContext {
    #[inline]
    pub fn instance(&self) -> &Instance {
        &self.instance
    }

    #[inline]
    pub fn surface(&self) -> &Surface {
        &self.surface
    }

    #[inline]
    pub fn surface_khr(&self) -> vk::SurfaceKHR {
        self.surface_khr
    }

    #[inline]
    pub fn physical_device(&self) -> vk::PhysicalDevice {
        self.physical_device
    }

    #[inline]
    pub const fn device(&self) -> &Device {
        &self.device
    }
}

impl VkContext {
    #[inline]
    pub unsafe fn get_mem_properties(&self) -> vk::PhysicalDeviceMemoryProperties {
        self.instance
            .get_physical_device_memory_properties(self.physical_device)
    }
}

impl VkContext {
    #[inline]
    pub fn new(
        entry: Entry,
        instance: Instance,
        debug_report_callback: Option<(DebugReport, vk::DebugReportCallbackEXT)>,
        surface: Surface,
        surface_khr: vk::SurfaceKHR,
        physical_device: vk::PhysicalDevice,
        device: Device,
    ) -> Self {
        VkContext {
            _entry: entry,
            instance,
            debug_report_callback,
            surface,
            surface_khr,
            physical_device,
            device,
        }
    }
    #[inline]
    pub unsafe fn new_staging_buf(&self, size: u64) -> Buffer {
        Buffer::new(
            self,
            size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        )
    }
    #[inline]
    pub unsafe fn new_dev_loc_buf(&self, size: u64, usage: vk::BufferUsageFlags) -> Buffer {
        Buffer::new(
            self,
            size,
            vk::BufferUsageFlags::TRANSFER_DST | usage,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        )
    }
}

impl Drop for VkContext {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_device(None);
            self.surface.destroy_surface(self.surface_khr, None);
            if let Some((report, callback)) = self.debug_report_callback.take() {
                report.destroy_debug_report_callback(callback, None);
            }
            self.instance.destroy_instance(None);
        }
    }
}
